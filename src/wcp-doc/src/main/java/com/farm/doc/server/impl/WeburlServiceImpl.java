package com.farm.doc.server.impl;

import java.util.List;

import com.farm.doc.domain.Weburl;
import com.farm.core.time.TimeTool;

import org.apache.log4j.Logger;

import com.farm.doc.dao.WeburlDaoInter;
import com.farm.doc.server.WeburlServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：推荐服务服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class WeburlServiceImpl implements WeburlServiceInter {
	@Resource
	private WeburlDaoInter weburlDaoImpl;

	private static final Logger log = Logger.getLogger(WeburlServiceImpl.class);

	@Override
	@Transactional
	public Weburl insertWeburlEntity(Weburl entity, LoginUser user) {
		entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setCusername(user.getName());
		entity.setEuser(user.getId());
		entity.setEusername(user.getName());
		entity.setEtime(TimeTool.getTimeDate14());
		entity.setPstate("1");
		return weburlDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Weburl editWeburlEntity(Weburl entity, LoginUser user) {
		Weburl entity2 = weburlDaoImpl.getEntity(entity.getId());
		entity2.setWebname(entity.getWebname());
		entity2.setUrl(entity.getUrl());
		entity2.setEusername(user.getName());
		entity2.setEuser(user.getId());
		entity2.setEtime(TimeTool.getTimeDate12());
		weburlDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteWeburlEntity(String id, LoginUser user) {

		weburlDaoImpl.deleteEntity(weburlDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Weburl getWeburlEntity(String id) {

		if (id == null) {
			return null;
		}
		return weburlDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createWeburlSimpleQuery(DataQuery query) {

		DataQuery dbQuery = DataQuery
				.init(query, "FARM_WEBURL",
						"ID,WEBNAME,URL,CTIME,ETIME,CUSERNAME,CUSER,EUSERNAME,EUSER,PCONTENT,PSTATE");
		return dbQuery;
	}

	// ----------------------------------------------------------------------------------
	public WeburlDaoInter getWeburlDaoImpl() {
		return weburlDaoImpl;
	}

	public void setWeburlDaoImpl(WeburlDaoInter dao) {
		this.weburlDaoImpl = dao;
	}

	@Override
	@Transactional
	public List<Weburl> getList() {
		return weburlDaoImpl.selectEntitys(new DBRule("1", "1", "=").getDBRules());
	}

}
