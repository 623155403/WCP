package com.farm.doc.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.farm.core.page.ViewMode;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.doc.domain.Doc;
import com.farm.doc.domain.ex.DocEntire;
import com.farm.doc.server.FarmDocManagerInter;
import com.farm.doc.server.FarmDocOperateRightInter;
import com.farm.doc.server.FarmDocgroupManagerInter;
import com.farm.doc.util.LuceneDocUtil;
import com.farm.lucene.FarmLuceneFace;
import com.farm.lucene.server.DocIndexInter;
import com.farm.parameter.FarmParameterService;
import com.farm.web.WebUtils;

/**
 * 文档管理
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/lucene")
@Controller
public class ActionFarmLuceneQuery extends WebUtils {
	@Resource
	private FarmDocManagerInter farmDocManagerImpl;
	@Resource
	private FarmDocOperateRightInter farmDocOperateRightImpl;
	@Resource
	private FarmDocgroupManagerInter farmDocgroupManagerImpl;
	private static final Logger log = Logger.getLogger(ActionFarmLuceneQuery.class);
	private int doNum;

	/**
	 * 重做索引
	 * 
	 * @return
	 */
	@RequestMapping("/reIndex")
	@ResponseBody
	public Map<String, Object> reIndex(DataQuery query, HttpServletRequest request) {
		int currentDocs = 0;
		DataResult result;
		query = DataQuery.init(query, "farm_doc", "id,title");
		query.setCurrentPage(1);
		query.addRule(new DBRule("STATE", "1", "="));
		query.addRule(new DBRule("READPOP", "1", "="));
		int i = 1;
		doNum = 0;
		int doNum_error = 0;
		DocIndexInter indexKnow = null;
		DocIndexInter indexWebsite = null;
		DocIndexInter indexWebFile = null;
		try {
			FarmLuceneFace luceneImpl = FarmLuceneFace.inctance();
			indexKnow = luceneImpl.getDocIndex(
					luceneImpl.getIndexPathFile(FarmParameterService.getInstance().getParameter("LUCENE_DIR_KNOW")));
			indexWebsite = luceneImpl.getDocIndex(
					luceneImpl.getIndexPathFile(FarmParameterService.getInstance().getParameter("LUCENE_DIR_SITE")));
			indexWebFile = luceneImpl.getDocIndex(
					luceneImpl.getIndexPathFile(FarmParameterService.getInstance().getParameter("LUCENE_DIR_FILE")));
			while (true) {
				try {
					query.setPagesize(100);
					query.setCurrentPage(i++);
					result = query.search();
					if (result.getResultList().size() <= 0) {
						break;
					}
					for (Map<String, Object> node : result.getResultList()) {
						DocEntire doc = farmDocManagerImpl.getDoc(node.get("ID").toString());
						DocIndexInter curentIndex = null;
						curentIndex = indexKnow;
						if (doc.getDoc().getDomtype().equals("3")) {
							curentIndex = indexWebsite;
						}
						if (doc.getDoc().getDomtype().equals("5")) {
							curentIndex = indexWebFile;
						}
						try {
							currentDocs++;
							curentIndex.deleteFhysicsIndex(doc.getDoc().getId());
							if ("1".equals(doc.getDoc().getReadpop()) && "1".equals(doc.getDoc().getState())) {
								curentIndex.indexDoc(LuceneDocUtil.getDocMap(doc));
							}
							doNum++;
						} catch (Exception e) {
							e.printStackTrace();
							doNum_error++;
							continue;
						}
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				indexKnow.close();
				indexWebsite.close();
			} catch (Exception e) {
			}
		}
		return ViewMode.getInstance().putAttr("num", 0).returnObjMode();
	}

	/**
	 * 获取当前索引状态
	 * 
	 * @return
	 */
	@RequestMapping("/indexPersont")
	@ResponseBody
	public Map<String, Object> loadIndexStatr() {
		return ViewMode.getInstance()
				.putAttr("cdocs", doNum).returnObjMode();
	}

}
