package com.farm.doc.controller;


import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/**
 * 全文索引
 * 
 * @author zhanghc
 * 
 */
@RequestMapping("/docIndex")
@Controller
public class ActionFarmDocIndexQuery extends WebUtils {
	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("doc/DocIndexLayout");
	}
}
