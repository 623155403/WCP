package com.farm.doc.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.farm.core.page.ViewMode;
import com.farm.doc.domain.FarmDocfile;
import com.farm.doc.server.FarmFileManagerInter;
import com.farm.doc.server.FarmFileManagerInter.FILE_TYPE;
import com.farm.doc.server.impl.FarmFileManagerImpl;
import com.farm.parameter.FarmParameterService;
import com.farm.web.WebUtils;
import com.farm.web.easyui.EasyUiUtils;

/**
 * 文档管理
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/actionImg")
@Controller
public class ActionImgQuery extends WebUtils {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(ActionImgQuery.class);
	// private File imgFile;
	// private int error;
	// private String url;
	// private String message;
	// private InputStream inputStream;
	// private String id;
	// private String filename;
	// private String imgFileFileName;
	// private String imgFileContentType;

	@Resource
	private FarmFileManagerInter farmFileManagerImpl;

	//
	// @Override
	// public void addActionError(String anErrorMessage) {
	// // 这里要先判断一下，是我们要替换的错误，才处理
	// if (anErrorMessage
	// .startsWith("Request exceeded allowed size limit! Max size allowed is"))
	// {
	// Pattern pattern = Pattern.compile("[,|\\d]+");
	// Matcher m = pattern.matcher(anErrorMessage);
	// // 匹配一次
	// m.find();
	// String s1 = m.group().replaceAll(",", "");// 上传的文件大小
	// // 再匹配一次
	// m.find();
	// String s2 = m.group().replaceAll(",", "");// 所限制的大小
	// String fileUploadErrorMessage = null;
	// if (!s1.equals("") && !s2.equals("")) {
	// fileUploadErrorMessage = "你上传的文件大小（" + Long.valueOf(s2) / 1024
	// / 1024 + "M）超过允许的大小（" + Long.valueOf(s1) / 1024 / 1024
	// + "M）";
	// getRequest().setAttribute("fileUploadErrorMessage",
	// fileUploadErrorMessage);
	// // 将信息替换掉
	// super.addActionError(fileUploadErrorMessage);
	// }
	// } else {// 否则按原来的方法处理
	// super.addActionError(anErrorMessage);
	// }
	// }
	//
	/**
	 * 上传文件
	 * 
	 * @return
	 */
	@RequestMapping(value = "/PubFPupload.do")
	@ResponseBody
	public Map<String, Object> upload(@RequestParam(value = "imgFile", required = false) MultipartFile file,
			HttpServletRequest request, ModelMap model, HttpSession session) {
		int error;
		String message;
		String url = null;
		String id = null;
		String fileName = "";
		try {
			String fileid = null;
			// 验证大小
			String maxLength = FarmParameterService.getInstance().getParameter("config.doc.upload.length.max");
			if (file.getSize() > Integer.valueOf(maxLength)) {
				throw new Exception("文件不能超过" + Integer.valueOf(maxLength) / 1024 + "kb");
			}
			CommonsMultipartFile cmFile = (CommonsMultipartFile) file;
			DiskFileItem item = (DiskFileItem) cmFile.getFileItem();
			{//小于8k不生成到临时文件，临时解决办法。zhanghc20150919
				if(!item.getStoreLocation().exists()){
					item.write(item.getStoreLocation());
				}
			}
			
			fileName = item.getName();
			// 验证类型
			List<String> types = parseIds(FarmParameterService.getInstance().getParameter("config.doc.upload.types")
					.toUpperCase().replaceAll("，", ","));
			if (!types.contains(file.getOriginalFilename()
					.substring(file.getOriginalFilename().lastIndexOf(".") + 1, file.getOriginalFilename().length())
					.toUpperCase())) {
				throw new Exception("文件类型错误，允许的类型为：" + FarmParameterService.getInstance()
						.getParameter("config.doc.upload.types").toUpperCase().replaceAll("，", ","));
			}
			fileid = farmFileManagerImpl.saveFile(item.getStoreLocation(), FILE_TYPE.HTML_INNER_IMG,
					file.getOriginalFilename(), getCurrentUser(session));
			error = 0;
			url = farmFileManagerImpl.getFileURL(fileid);
			message = "";
			id = fileid;
		} catch (Exception e) {
			e.printStackTrace();
			error = 1;
			message = e.getMessage();
		}
		return ViewMode.getInstance()
				.putAttr("error", error)
				.putAttr("url", url)
				.putAttr("message", message)
				.putAttr("id", id)
				.putAttr("fileName", fileName)
				.returnObjMode();
	}

	/**
	 * 下载文件
	 * 
	 * @return
	 */
	@RequestMapping("/Publoadfile")
	public void download(String id, HttpServletRequest request, HttpServletResponse response) {
		FarmDocfile file = null;
		try {
			file = farmFileManagerImpl.getFile(id);
		} catch (Exception e) {
			file = null;
		}
		if (file == null || file.getFile() == null || !file.getFile().exists()) {
			file = new FarmDocfile();
			file.setFile(farmFileManagerImpl.getNoneImg());
			file.setName("none");
		}
		response.setCharacterEncoding("utf-8");
		response.setContentType("multipart/form-data");
		response.setHeader("Content-Disposition", "attachment;fileName=" + file.getName());
		try {
			InputStream is = new FileInputStream(file.getFile());
			OutputStream os = response.getOutputStream();
			byte[] b = new byte[2048];
			int length;
			while ((length = is.read(b)) > 0) {
				os.write(b, 0, length);
			}
			// 这里主要关闭。
			os.close();
			is.close();
		} catch (FileNotFoundException e) {
			try {
				String webPath = FarmParameterService.getInstance().getParameter("farm.constant.webroot.path");
				String filePath = "/WEB-FACE/img/style/nullImg.png".replaceAll("/",
						File.separator.equals("/") ? "/" : "\\\\");
				File nullFile = new File(webPath + filePath);
				InputStream is = new FileInputStream(nullFile);
				OutputStream os = response.getOutputStream();
				byte[] b = new byte[2048];
				int length;
				while ((length = is.read(b)) > 0) {
					os.write(b, 0, length);
				}
				// 这里主要关闭。
				os.close();
				is.close();
			} catch (Exception e1) {
				log.error(e.getMessage());
			}
			log.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// private final static FarmFileManagerInter fileIMP =
	// (FarmFileManagerInter) BeanFactory
	// .getBean("farm_docFileProxyId");
	//
	// public File getImgFile() {
	// return imgFile;
	// }
	//
	// public void setImgFile(File imgFile) {
	// this.imgFile = imgFile;
	// }
	//
	// public int getError() {
	// return error;
	// }
	//
	// public void setError(int error) {
	// this.error = error;
	// }
	//
	// public String getUrl() {
	// return url;
	// }
	//
	// public void setUrl(String url) {
	// this.url = url;
	// }
	//
	// public String getMessage() {
	// return message;
	// }
	//
	// public void setMessage(String message) {
	// this.message = message;
	// }
	//
	// public InputStream getInputStream() {
	// return inputStream;
	// }
	//
	// public void setInputStream(InputStream inputStream) {
	// this.inputStream = inputStream;
	// }
	//
	// public String getId() {
	// return id;
	// }
	//
	// public void setId(String id) {
	// this.id = id;
	// }
	//
	// public String getFilename() {
	// try {
	// filename = new String(filename.getBytes("GBK"), "ISO-8859-1");
	// } catch (UnsupportedEncodingException e) {
	//
	// }// 中文乱码解决
	// return filename;
	// }
	//
	// public void setFilename(String filename) {
	// this.filename = filename;
	// }
	//
	// public String getImgFileFileName() {
	// return imgFileFileName;
	// }
	//
	// public void setImgFileFileName(String imgFileFileName) {
	// this.imgFileFileName = imgFileFileName;
	// }
	//
	// public String getImgFileContentType() {
	// return imgFileContentType;
	// }
	//
	// public void setImgFileContentType(String imgFileContentType) {
	// this.imgFileContentType = imgFileContentType;
	// }

}
