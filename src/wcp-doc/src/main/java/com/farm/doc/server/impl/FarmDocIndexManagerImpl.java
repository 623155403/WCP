package com.farm.doc.server.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.farm.core.page.ViewMode;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.result.ResultsHandle;
import com.farm.doc.dao.FarmDocDaoInter;
import com.farm.doc.dao.FarmDocenjoyDaoInter;
import com.farm.doc.dao.FarmDocfileDaoInter;
import com.farm.doc.dao.FarmDocgroupDaoInter;
import com.farm.doc.dao.FarmDocmessageDaoInter;
import com.farm.doc.dao.FarmDocruninfoDaoInter;
import com.farm.doc.dao.FarmDocruninfoDetailDaoInter;
import com.farm.doc.dao.FarmDoctextDaoInter;
import com.farm.doc.dao.FarmDoctypeDaoInter;
import com.farm.doc.dao.FarmRfDoctextfileDaoInter;
import com.farm.doc.dao.FarmRfDoctypeDaoInter;
import com.farm.doc.dao.FarmtopDaoInter;
import com.farm.doc.domain.Doc;
import com.farm.doc.domain.ex.DocBrief;
import com.farm.doc.server.FarmDocIndexInter;
import com.farm.doc.server.FarmDocOperateRightInter;
import com.farm.doc.server.FarmDocgroupManagerInter;
import com.farm.doc.server.FarmFileManagerInter;
import com.farm.doc.util.HtmlUtils;
import com.farm.lucene.FarmLuceneFace;
import com.farm.lucene.server.DocQueryImpl;
import com.farm.lucene.server.DocQueryInter;
import com.farm.parameter.FarmParameterService;
import com.farm.util.web.FarmFormatUnits;
import com.farm.util.web.WebHotCase;

@Service
public class FarmDocIndexManagerImpl implements FarmDocIndexInter {
	@Resource
	private FarmDocDaoInter farmDocDao;
	@Resource
	private FarmDocfileDaoInter farmDocfileDao;
	@Resource
	private FarmDoctextDaoInter farmDoctextDao;
	@Resource
	private FarmRfDoctextfileDaoInter farmRfDoctextfileDao;
	@Resource
	private FarmRfDoctypeDaoInter farmRfDoctypeDao;
	@Resource
	private FarmDoctypeDaoInter farmDoctypeDao;
	@Resource
	private FarmDocmessageDaoInter farmDocmessageDao;
	@Resource
	private FarmDocruninfoDaoInter farmDocruninfoDao;
	@Resource
	private FarmDocenjoyDaoInter farmDocenjoyDao;
	@Resource
	private FarmDocOperateRightInter farmDocOperate;
	@Resource
	private FarmDocgroupDaoInter farmDocgroupDao;
	@Resource
	private FarmDocruninfoDetailDaoInter farmDocruninfoDetailDao;
	@Resource
	private FarmFileManagerInter farmFileServer;
	@Resource
	private FarmDocgroupManagerInter farmDocgroupManagerImpl;
	@Resource
	private FarmtopDaoInter farmtopDaoImpl;
	private final static Logger log = Logger.getLogger(FarmDocIndexManagerImpl.class);

	@Override
	@Transactional
	public List<DocBrief> getRelationDocs(String docid, int num) {
		Doc doc = farmDocDao.getEntity(docid);
		List<File> file = new ArrayList<File>();
		file.add(FarmLuceneFace.inctance()
				.getIndexPathFile(FarmParameterService.getInstance().getParameter("LUCENE_DIR_FILE")));
		file.add(FarmLuceneFace.inctance()
				.getIndexPathFile(FarmParameterService.getInstance().getParameter("LUCENE_DIR_KNOW")));
		file.add(FarmLuceneFace.inctance()
				.getIndexPathFile(FarmParameterService.getInstance().getParameter("LUCENE_DIR_SITE")));
		DocQueryInter query = DocQueryImpl.getInstance(file);
		String iql = null;
		if (iql == null) {
			iql = "WHERE(TITLE,TEXT=" + HtmlUtils.HtmlRemoveTag(doc.getTitle().trim()).replaceAll(":", "") + ")";
		}
		try {
			DataResult result = query.queryByMultiIndex(iql, 1, num).getDataResult();
			result.runHandle((new ResultsHandle() {
				@Override
				public void handle(Map<String, Object> row) {
					row.put("DOCID", row.get("ID"));
				}
			}));
			return result.getObjectList(DocBrief.class);
		} catch (Exception e) {
			log.error(e.toString());
			return new ArrayList<>();
		}
	}

	@Override
	public DataResult search(String word, String userid, Integer pagenum) throws Exception {
		// TITLE,PUBTIME,VISITNUM,TYPENAME,AUTHOR,TAGKEY,DOCDESCRIBE,TEXT
		List<File> files = new ArrayList<File>();
		if (userid != null) {
			DataResult groups = farmDocgroupManagerImpl.getGroupsByUser(userid, 1000, 1);
			for (Map<String, Object> node : groups.getResultList()) {
				File file = FarmLuceneFace.inctance().getIndexPathFile("GROUP" + (String) node.get("ID"));
				if (file.isDirectory()) {
					files.add(file);
				}
			}
		}
		files.add(FarmLuceneFace.inctance()
				.getIndexPathFile(FarmParameterService.getInstance().getParameter("LUCENE_DIR_KNOW")));
		files.add(FarmLuceneFace.inctance()
				.getIndexPathFile(FarmParameterService.getInstance().getParameter("LUCENE_DIR_SITE")));
		files.add(FarmLuceneFace.inctance()
				.getIndexPathFile(FarmParameterService.getInstance().getParameter("LUCENE_DIR_FILE")));
		DocQueryInter query = DocQueryImpl.getInstance(files);
		String iql = null;
		word = HtmlUtils.HtmlRemoveTag(word.trim());
		if (word.indexOf("TYPE:") >= 0 && iql == null) {
			word = word.substring(word.indexOf("TYPE:") + 5).replaceAll(":", "");
			iql = "WHERE(TYPENAME=" + word + ")";
		}
		if (word.indexOf("TAG:") >= 0 && iql == null) {
			word = word.substring(word.indexOf("TYPE:") + 5).replaceAll(":", "");
			iql = "WHERE(TAGKEY=" + word + ")";
		}
		if (word.indexOf("AUTHOR:") >= 0 && iql == null) {
			word = word.substring(word.indexOf("AUTHOR:") + 7).replaceAll(":", "");
			iql = "WHERE(AUTHOR=" + word + ")";
		}
		if (word.indexOf("TITLE:") >= 0 && iql == null) {
			word = word.substring(word.indexOf("TITLE:") + 6).replaceAll(":", "");
			iql = "WHERE(TITLE=" + word + ")";
		}
		if (iql == null) {
			// word.substring(word.indexOf("TYPE:"));
			iql = "WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=" + word.replaceAll(":", "") + ")";
		}
		if (pagenum == null) {
			pagenum = 1;
		}
		WebHotCase.putCase(word);
		DataResult result = query.queryByMultiIndex(iql, pagenum, 10).getDataResult();
		for (Map<String, Object> node : result.getResultList()) {
			node.put("PUBTIME", FarmFormatUnits.getFormateTime(node.get("PUBTIME").toString(), true));
			String tags = node.get("TAGKEY").toString();
			String text = node.get("TEXT").toString();
			node.put("DOCDESCRIBE", text.length() > 256 ? text.substring(0, 256) : text);
			if (tags != null && tags.trim().length() > 0) {
				String[] tags1 = tags.trim().replaceAll("，", ",").replaceAll("、", ",").split(",");
				node.put("TAGKEY", Arrays.asList(tags1));
			} else {
				node.put("TAGKEY", new ArrayList<String>());
			}
		}
		return result;
	}
}
