package com.farm.doc.server;

import java.util.List;

import com.farm.core.sql.result.DataResult;
import com.farm.doc.domain.ex.DocBrief;

/**
 * 文档全文检索服务
 * 
 * @author 王东
 * @version 20140902
 */
public interface FarmDocIndexInter {

	/**
	 * 查询相关知识
	 * 
	 * @param docid
	 * @param num
	 * @return
	 */
	List<DocBrief> getRelationDocs(String docid, int num);

	/**
	 * 全文检索
	 * 
	 * @param word
	 * @param pagenum
	 * @return
	 */
	DataResult search(String word,String userid, Integer pagenum) throws Exception;
}
