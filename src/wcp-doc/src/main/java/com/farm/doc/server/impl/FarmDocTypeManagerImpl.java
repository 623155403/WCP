package com.farm.doc.server.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.farm.core.auth.domain.LoginUser;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.query.DataQuery.CACHE_UNIT;
import com.farm.core.sql.result.DataResult;
import com.farm.core.time.TimeTool;
import com.farm.doc.dao.FarmDocDaoInter;
import com.farm.doc.dao.FarmDocenjoyDaoInter;
import com.farm.doc.dao.FarmDocfileDaoInter;
import com.farm.doc.dao.FarmDocgroupDaoInter;
import com.farm.doc.dao.FarmDocmessageDaoInter;
import com.farm.doc.dao.FarmDocruninfoDaoInter;
import com.farm.doc.dao.FarmDocruninfoDetailDaoInter;
import com.farm.doc.dao.FarmDoctextDaoInter;
import com.farm.doc.dao.FarmDoctypeDaoInter;
import com.farm.doc.dao.FarmRfDoctextfileDaoInter;
import com.farm.doc.dao.FarmRfDoctypeDaoInter;
import com.farm.doc.dao.FarmtopDaoInter;
import com.farm.doc.domain.FarmDoctype;
import com.farm.doc.domain.ex.TypeBrief;
import com.farm.doc.server.FarmDocOperateRightInter;
import com.farm.doc.server.FarmDocTypeInter;
import com.farm.doc.server.FarmFileManagerInter;
import com.farm.parameter.FarmParameterService;

/**
 * 文档管理
 * 
 * @author MAC_wd
 */
@Service
public class FarmDocTypeManagerImpl implements FarmDocTypeInter {
	@Resource
	private FarmDocDaoInter farmDocDao;
	@Resource
	private FarmDocfileDaoInter farmDocfileDao;
	@Resource
	private FarmDoctextDaoInter farmDoctextDao;
	@Resource
	private FarmRfDoctextfileDaoInter farmRfDoctextfileDao;
	@Resource
	private FarmRfDoctypeDaoInter farmRfDoctypeDao;
	@Resource
	private FarmDoctypeDaoInter farmDoctypeDao;
	@Resource
	private FarmDocmessageDaoInter farmDocmessageDao;
	@Resource
	private FarmDocruninfoDaoInter farmDocruninfoDao;
	@Resource
	private FarmDocenjoyDaoInter farmDocenjoyDao;
	@Resource
	private FarmDocOperateRightInter farmDocOperate;
	@Resource
	private FarmDocgroupDaoInter farmDocgroupDao;
	@Resource
	private FarmDocruninfoDetailDaoInter farmDocruninfoDetailDao;
	@Resource
	private FarmFileManagerInter farmFileServer;
	@Resource
	private FarmtopDaoInter farmtopDaoImpl;
	private static final Logger log = Logger.getLogger(FarmDocTypeManagerImpl.class);

	@Transactional
	public FarmDoctype insertType(FarmDoctype entity, LoginUser user) {
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setEtime(TimeTool.getTimeDate14());
		entity.setCuser(user.getId());
		entity.setEuser(user.getId());
		entity.setCusername(user.getName());
		entity.setEusername(user.getName());
		if (entity.getParentid() == null || entity.getParentid().trim().length() <= 0) {
			entity.setParentid("NONE");
		}
		entity.setTreecode("NONE");
		entity = farmDoctypeDao.insertEntity(entity);
		if (entity.getParentid().equals("NONE")) {
			entity.setTreecode(entity.getId());
		} else {
			entity.setTreecode(farmDoctypeDao.getEntity(entity.getParentid()).getTreecode() + entity.getId());
		}
		return farmDoctypeDao.insertEntity(entity);
	}

	@Transactional
	public FarmDoctype editType(FarmDoctype entity, LoginUser user) {
		FarmDoctype entity2 = farmDoctypeDao.getEntity(entity.getId());
		entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setEuser(user.getId());
		entity2.setEusername(user.getName());
		entity2.setName(entity.getName());
		entity2.setTypemod(entity.getTypemod());
		entity2.setContentmod(entity.getContentmod());
		entity2.setSort(entity.getSort());
		entity2.setTags(entity.getTags());
		entity2.setType(entity.getType());
		entity2.setMetatitle(entity.getMetatitle());
		entity2.setMetakey(entity.getMetakey());
		entity2.setMetacontent(entity.getMetacontent());
		entity2.setLinkurl(entity.getLinkurl());
		entity2.setPcontent(entity.getPcontent());
		entity2.setPstate(entity.getPstate());
		farmDoctypeDao.editEntity(entity2);
		return entity2;
	}

	@Transactional
	public void deleteType(String typeId, LoginUser user) {
		// 删除分类中间表
		List<DBRule> rulesDelType = new ArrayList<DBRule>();
		rulesDelType.add(new DBRule("TYPEID", typeId, "="));
		farmRfDoctypeDao.deleteEntitys(rulesDelType);
		farmDoctypeDao.deleteEntity(farmDoctypeDao.getEntity(typeId));
	}

	@Transactional
	public FarmDoctype getType(String id) {
		if (id == null) {
			return null;
		}
		return farmDoctypeDao.getEntity(id);
	}

	@Override
	@Transactional
	public List<FarmDoctype> getTypeAllParent(String typeid) {
		String id = typeid;
		List<FarmDoctype> types = new ArrayList<FarmDoctype>();
		while (id != null) {
			FarmDoctype centity = farmDoctypeDao.getEntity(id);
			if (centity == null || centity.getParentid() == null || centity.getParentid().trim().length() <= 0) {
				id = null;
			} else {
				id = centity.getParentid();

			}
			if (centity != null) {
				types.add(centity);
			}
		}
		Collections.reverse(types);
		return types;
	}

	@Override
	@Transactional
	public List<TypeBrief> getTypeInfos(String parentId) {
		DataQuery query = null;
		query = DataQuery.init(query,
				"(SELECT a.NAME as NAME, a.ID as ID, a.PARENTID as PARENTID, (SELECT COUNT(B1.ID) FROM FARM_DOC B1 LEFT JOIN FARM_RF_DOCTYPE B2 ON B1.ID = B2.DOCID LEFT JOIN FARM_DOCTYPE B3 ON B3.ID = B2.TYPEID WHERE B3.TREECODE  LIKE CONCAT(A.TREECODE,'%') AND B1.STATE='1') AS NUM FROM farm_doctype AS a LEFT JOIN farm_doctype AS b ON b.ID = a.PARENTID WHERE 1 = 1 AND (a.TYPE = '1' OR a.TYPE = '3') AND a.PSTATE = '1' and (a.PARENTID='"
						+ parentId + "' or b.PARENTID='" + parentId + "') ORDER BY a.SORT ASC) AS e",
				"NAME,ID,PARENTID,NUM");
		query.setDistinct(true);
		query.setPagesize(1000);
		query.setNoCount();
		query.setCache(Integer.valueOf(FarmParameterService.getInstance().getParameter("config.wcp.cache.brief")),
				CACHE_UNIT.second);
		try {
			return query.search().getObjectList(TypeBrief.class);
		} catch (SQLException e) {
			log.error(e.toString());
			return new ArrayList<TypeBrief>();
		}
	}

	@Override
	@Transactional
	public List<TypeBrief> getPubTypes() {
		DataQuery query = DataQuery.init(new DataQuery(),
				"(SELECT a.NAME as NAME, a.ID as ID, a.PARENTID as PARENTID, (SELECT COUNT(B1.ID) FROM FARM_DOC B1 LEFT JOIN FARM_RF_DOCTYPE B2 ON B1.ID = B2.DOCID LEFT JOIN FARM_DOCTYPE B3 ON B3.ID = B2.TYPEID WHERE B3.TREECODE  LIKE CONCAT(A.TREECODE,'%') AND B1.STATE='1') AS NUM FROM farm_doctype AS a WHERE 1 = 1 AND (TYPE = '1' OR TYPE = '3') AND PSTATE = '1' ORDER BY SORT ASC) AS e",
				"NAME,ID,PARENTID,NUM");
		query.setPagesize(1000);
		query.setNoCount();
		query.setCache(Integer.valueOf(FarmParameterService.getInstance().getParameter("config.wcp.cache.brief")),
				CACHE_UNIT.second);
		try {
			return query.search().getObjectList(TypeBrief.class);
		} catch (SQLException e) {
			log.error(e.toString());
			return new ArrayList<TypeBrief>();
		}
	}

	@Override
	@Transactional
	public DataResult getTypeDocs(String userid, String typeid, int pagesize, int currentPage) {
		FarmDoctype type = getType(typeid);
		DataQuery query = DataQuery.init(new DataQuery(),
				"farm_doc a LEFT JOIN farm_docruninfo b ON a.RUNINFOID=b.ID LEFT JOIN farm_rf_doctype c ON c.DOCID=a.ID LEFT JOIN farm_doctype d ON d.ID=c.TYPEID LEFT JOIN ALONE_AUTH_USER e ON e.ID = a.CUSER",
				"a.ID as DOCID,a.DOMTYPE as DOMTYPE,a.TITLE AS title,a.DOCDESCRIBE AS DOCDESCRIBE,a.AUTHOR AS AUTHOR,a.PUBTIME AS PUBTIME,a.TAGKEY AS TAGKEY ,a.IMGID AS IMGID,b.VISITNUM AS VISITNUM,b.PRAISEYES AS PRAISEYES,b.PRAISENO AS PRAISENO,b.HOTNUM AS HOTNUM,b.EVALUATE as EVALUATE,b.ANSWERINGNUM as ANSWERINGNUM,d.id as TYPEID,d.NAME AS TYPENAME, e.ID as USERID, e.NAME as USERNAME, e.IMGID as USERIMGID");
		query.addSort(new DBSort("a.etime", "desc"));
		query.setCurrentPage(currentPage);
		query.addRule(new DBRule("a.STATE", "1", "="));
		query.addRule(new DBRule("DOMTYPE", "4", "!="));
		if (type != null) {
			query.addRule(new DBRule("d.TREECODE", type.getTreecode(), "like-"));
		}
		// 文章三种情况判断
		// 1.文章阅读权限为公共
		// 2.文章的创建者为当前登录用户
		// 3.文章的阅读权限为小组，并且当前登陆用户为组内成员.(使用子查询处理)
		query.addSqlRule("and (a.READPOP='1' or a.CUSER='" + userid
				+ "' OR (a.READPOP = '2' AND 0 < (SELECT e.ID FROM farm_docgroup_user e WHERE e.PSTATE = 1 AND e.GROUPID = a.DOCGROUPID AND e.CUSER = '"
				+ userid + "')))");
		query.setPagesize(pagesize);
		DataResult docs = null;
		try {
			docs = query.search();
		} catch (SQLException e) {
			log.error(e.toString());
			return DataResult.getInstance();
		}
		for (Map<String, Object> map : docs.getResultList()) {
			if (map.get("USERIMGID") != null && !map.get("USERIMGID").toString().isEmpty()) {
				map.put("PHOTOURL", farmFileServer.getFileURL(map.get("USERIMGID").toString()));
			} else {
				map.put("PHOTOURL", farmFileServer.getFileURL("NONE"));
			}
		}
		return docs;
	}
}
