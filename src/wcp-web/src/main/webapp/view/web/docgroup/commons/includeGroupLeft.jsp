<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="/view/conf/farmdoc.tld" prefix="DOC"%>
<p>
	<c:forEach items="${docgroup.tags}" var="tag">
		<span class="label label-info tagsearch" title="${tag}"
			style="font-weight: lighter; font-size: 12px; cursor: pointer;">${tag}</span>
	</c:forEach>
</p>
<a class="thumbnail"> <c:if test="${docgroup.groupimg==null}">
		<img src="UI-WEB/docgroup/commons/imgDemo.png" alt="...">
	</c:if> <c:if test="${docgroup.groupimg!=null}">
		<img src="${docgroup.imgurl}" alt="...">
	</c:if>
</a>
<p>${docgroup.groupnote}</p>
<div class="row column_box">
	<div class="col-sm-12">
		<span class="glyphicon glyphicon-fire  column_title">管理员</span>
	</div>
</div>
<div class="col-sm-12">
	<hr class="hr_split" />
</div>
<div class="row">
	<c:forEach items="${docgroup.admins}" var="note">
		<div class="col-sm-6 col-md-4">
			<div class="thumbnail">
				<a href="webuser/PubHome.do?userid=${note.id}"
					style="font-size: 12px; margin: auto;"><img
					src="<DOC:ImgUrl fileid="${note.imgid}"/>" alt="..."></a>
				<div
					style="height: 32px; overflow: hidden; line-height: 14px; text-align: center;">
					<a href="webuser/PubHome.do?userid=${note.id}"
						style="font-size: 12px; margin: auto;">${note.name}</a>
				</div>
			</div>
		</div>
	</c:forEach>
</div>
<div class="row column_box">
	<div class="col-sm-12">
		<span class="glyphicon glyphicon-fire  column_title">小组成员</span>
	</div>
</div>
<div class="col-sm-12">
	<hr class="hr_split" />
</div>
<div class="row">
	<c:forEach items="${docgroup.users}" var="note">
		<div class="col-sm-6 col-md-4">
			<div class="thumbnail">
				<a href="webuser/PubHome.do?userid=${note.id}"
					style="font-size: 12px; margin: auto;"> <img
					src="<DOC:ImgUrl fileid="${note.imgid}"/>" alt="..."></a>
				<div
					style="height: 32px; overflow: hidden; line-height: 14px; text-align: center;">
					<a href="webuser/PubHome.do?userid=${note.id}"
						style="font-size: 12px; margin: auto;">${note.name}</a>
				</div>
			</div>
		</div>
	</c:forEach>
</div>
<script type="text/javascript">
	$(function() {
		//标签
		$('.tagsearch').bind('click', function() {
			luceneSearch('TYPE:' + $(this).attr('title'));
		});
	});
	function leaveGroupFunc(id) {
		//判断是否有别的管理员，没有的话提示用户
		if (confirm('是否确定退出小组？')) {
			if ('${usergroup.leadis}' != '1') {
				//如果用户非管理员则直接退出
				window.location = 'webgroup/leaveGroup.do?groupId=' + id;
				return false;
			}

			$.post('webgroup/haveAdministratorIs.htm', {
				"groupId" : id
			}, function(flag) {
				if (flag.adminNum > 1) {
					window.location = 'webgroup/leaveGroup.do?groupId=' + id;
				} else {
					if (confirm('如果退出该小组，该小组将没有任何管理员。是否确定退出？')) {
						window.location = 'webgroup/leaveGroup.do?groupId='
								+ id;
					}
				}
			});
		}
	}
//-->
</script>