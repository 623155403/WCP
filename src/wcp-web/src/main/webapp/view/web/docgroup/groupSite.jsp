<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${docgroup.groupname}小组首页-<PF:ParameterValue
		key="config.sys.title" />
</title>
<jsp:include page="/view/conf/include-web.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox">
		<div class="container ">
			<div class="row column_box">
				<div class="col-md-3  hidden-xs">
					<jsp:include page="commons/includeGroupLeft.jsp"></jsp:include>
				</div>
				<div class="col-md-9">
					<h3 style="text-align: center; font-weight: bold;">
						<c:if test="${docgroup.joincheck=='1'}">
							<span class="glyphicon glyphicon-lock">&nbsp;${docgroup.groupname}&nbsp;</span>
						</c:if>
						<c:if test="${docgroup.joincheck=='0'}">
								${docgroup.groupname}
							</c:if>
					</h3>
					<div class="bg-success">
						<small>小组成立于</small> <strong> <u> <PF:FormatTime
									date="${docgroup.ctime}" yyyyMMddHHmmss="yyyy-MM-dd HH:mm" /></u>
						</strong> <small>,有成员</small> <strong> <u>${docgroup.usernum}</u>
						</strong> <small>人,共发布文档</small> <strong> <u>${docnum}</u>
						</strong> <small>篇</small>
					</div>
					<jsp:include page="/view/web/operation/includeCreatOperate.jsp"></jsp:include>
					<div style="float: right;"><jsp:include
							page="/view/web/operation/includeGroupSiteOperate.jsp"></jsp:include></div>
					<div style="clear: both; margin: 4px;"></div>
					${home.texts.text1}
					<jsp:include page="commons/includeGroupSiteDocs.jsp"></jsp:include>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	var loadUrl;
	$(function() {
		loadUrl = 'index/FPLoadMygroupHome.htm';
		loadDocs(1);
		$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
			if ($(this).attr('id') == 'home') {
				loadUrl = 'index/FPLoadMygroupHome.htm';
				loadDocs(1);
			}
			if ($(this).attr('id') == 'new') {
				loadUrl = 'index/FPLoadGroupNewDoc.htm';
				loadDocs(1);
			}
			if ($(this).attr('id') == 'hot') {
				loadUrl = 'index/FPLoadGroupHotDoc.htm';
				loadDocs(1);
			}
			if ($(this).attr('id') == 'good') {
				loadUrl = 'index/FPLoadGroupGoodDoc.htm';
				loadDocs(1);
			}
			if ($(this).attr('id') == 'bad') {
				loadUrl = 'index/FPLoadGroupBadDoc.htm';
				loadDocs(1);
			}
		})
	});
	function loadDocs(page) {
		$('#resultBoxDivId').html('Loading...');
		$('#resultBoxDivId').load(loadUrl, {
			id : '${docgroup.id}',
			'query.currentPage' : page
		});
	}
</script>
</html>