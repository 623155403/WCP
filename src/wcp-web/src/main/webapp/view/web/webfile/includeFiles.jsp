<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="row">
	<c:forEach var="node" items="${DOCE.files}">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<div style="height: 128px;">
						<a href="${node.url}"><c:if
								test="${fn:toUpperCase(fn:replace(node.exname,'.',''))=='PNG'||fn:toUpperCase(fn:replace(node.exname,'.',''))=='JPG'||fn:toUpperCase(fn:replace(node.exname,'.',''))=='JPEG'||fn:toUpperCase(fn:replace(node.exname,'.',''))=='GIF'}">
								<img style="max-height: 128px; max-width: 128px;" alt="${node}"
									src="${node.url}" />
							</c:if> <c:if
								test="${fn:toUpperCase(fn:replace(node.exname,'.',''))!='PNG'&&fn:toUpperCase(fn:replace(node.exname,'.',''))!='JPG'&&fn:toUpperCase(fn:replace(node.exname,'.',''))!='JPEG'&&fn:toUpperCase(fn:replace(node.exname,'.',''))!='GIF'}">
								<img style="max-height: 128px; max-width: 128px;" alt="${node}"
									src="text/img/fileicon/${fn:toUpperCase(fn:replace(node.exname,'.',''))}.png" />
							</c:if></a> 
					</div>${node.len/1000}kb
				</div>
				<div class="panel-footer">
					<div style="overflow: hidden;height: 50px;">
						<a href="${node.url}">&nbsp;下载${node.name}</a>
					</div>
				</div>
			</div>
		</div>
	</c:forEach>
</div>
