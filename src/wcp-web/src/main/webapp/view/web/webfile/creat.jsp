<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmdoc.tld" prefix="DOC"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>上传资源文件-<PF:ParameterValue key="config.sys.title" /></title>
<jsp:include page="/view/conf/include-web.jsp"></jsp:include>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/themes/default/default.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="<PF:basePath/>text/lib/kindeditor/zh_CN.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
</head>
<style>
<!--
.ke-button-common {
	background-image: none;
	background-color: transparent;
	margin: 0px;
	width: 100px;
}

.ke-button {
	cursor: pointer;
	color: #fff;
	margin: 0px;
}
-->
</style>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox">
		<div class="container ">
			<div class="row  column_box">
				<div class="col-md-3  visible-lg visible-md"></div>
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li class="active">WCP</li>
								<li class="active">创建文件资源</li>
							</ol>
						</div>
					</div>
					<form role="form" action="webfile/addsubmit.do"
						id="knowSubmitFormId" method="post">
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading center-block">
										<input type="button" class="btn btn-info btn-xs center-block"
											style="padding: 0px;" id="uploadButton" value="上传资源" />
									</div>
									<div class="panel-body" id="fileListId">
										<%-- 
										<input type="hidden" name="fileId" id="uploadfileId"
											value="${fileId}" /> <span id="uploadMarkId">${fileName}</span> --%>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="exampleInputEmail1"> 标题 <span
										class="alertMsgClass">*</span>
									</label> <input type="text" class="form-control" name="knowtitle"
										value="${doc.doc.title}" id="knowtitleId" placeholder="输入知识标题" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1"> tag </label> <input
										type="text" class="form-control" id="knowtagId"
										value="${doc.doc.tagkey}" name="knowtag"
										placeholder="输入类别标签(如果没有系统将自动创建)" />
								</div>

							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">
										文档分类
										<span class="alertMsgClass">*</span>
									</label>
									<div class="row">
										<div class="col-md-9">
											<input type="text" class="form-control" id="knowtypeTitleId"
												readonly="readonly" placeholder="选择文档分类"
												value="${doc.type.name}" />
											<input type="hidden" name="knowtype" id="knowtypeId"
												value="${doc.type.id}" />
										</div>
										<div class="col-md-3">
											<button class="btn btn-info btn-sm" data-toggle="modal"
												id="openChooseTypeButtonId" data-target="#myModal">
												选择</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="exampleInputEmail1"> 是否发布到小组 </label> <select
										class="form-control" name="docgroup" id="docgroupId"
										val="${doc.doc.docgroupid!=null?(doc.doc.docgroupid):docgroup}">
										<option value="0">否</option>
										<DOC:docGroupOption aroundS="[工作小组]:" />
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="exampleInputEmail1"> 编辑权限 <span
										class="alertMsgClass">*</span>
									</label> <select class="form-control" name="writetype" id="writetypeId"
										val="0">
										<option value="">~请选择~</option>
										<option value="0">创建人</option>
										<option value="1">公开</option>
										<option value="2">小组</option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="exampleInputEmail1"> 阅读权限 <span
										class="alertMsgClass">*</span>
									</label> <select class="form-control" name="readtype" id="readtypeId"
										val="1">
										<option value="">~请选择~</option>
										<option value="0">创建人</option>
										<option value="1">公开</option>
										<option value="2">小组</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label for="exampleInputEmail1"> 摘要 </label>
								<textarea name="text" id="contentId"
									style="height: 50px; width: 100%;">${text}</textarea>
							</div>
						</div>
						<br /> <br />
						<div class="row">
							<div class="col-md-2">
								<button type="button" id="knowSubmitButtonId"
									class="btn btn-info">提交</button>
							</div>
							<div class="col-md-10">
								<span class="alertMsgClass" id="errormessageShowboxId"></span>
							</div>
						</div>
						<div class="row column_box">
							<div class="col-md-12"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择分类</h4>
				</div>
				<div class="modal-body" id="treeChooseTypeBoxId">
					<ul class="doctypeUl">
						<c:forEach var="node" items="${types}">
							<c:if test="${node.parentid=='NONE'}">
								<li>
									<h5 class="showLableType">
										<a id="${node.id}">${node.name}</a>
										<c:if test="${node.num>0}">
											<span style="color: #D9534F; font-weight: bold;">${node.num}</span>
										</c:if>
									</h5>
									<ul>
										<c:forEach var="node1" items="${types}">
											<c:if test="${node1.parentid==node.id}">
												<li>
													<h5 class="showLableType">
														<a id="${node1.id}">${node1.name}</a>
														<c:if test="${node1.num>0}">
															<span style="color: #D9534F; font-weight: bold;">${node1.num}</span>
														</c:if>
													</h5>
													<ul class="list-inline">
														<c:forEach var="node2" items="${types}">
															<c:if test="${node2.parentid==node1.id}">
																<li>
																	<h5 class="showLableType">
																		<span style="color: #5CB85C;"><a
																			id="${node2.id}"> ${node2.name}</a></span>
																		<c:if test="${node2.num>0}">
																			<span style="color: #D9534F; font-weight: bold;">${node2.num}</span>
																		</c:if>
																	</h5>
																</li>
															</c:if>
														</c:forEach>
													</ul>
												</li>
											</c:if>
										</c:forEach>
									</ul>
								</li>
							</c:if>
						</c:forEach>
					</ul>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						关闭</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<jsp:include page="../commons/foot.jsp"></jsp:include><a onclick=""></a>
</body>
<script>
	KindEditor.ready(function(K) {
		var uploadbutton = K.uploadbutton({
			button : K('#uploadButton')[0],
			fieldName : 'imgFile',
			url : basePath + 'actionImg/PubFPupload.do',
			afterUpload : function(data) {
				if (data.error === 0) {
					$('#fileListId').append('<div id="file_'+data.id+'">');
					$('#file_'+data.id).append('<input type="hidden" name="fileId" value="'+data.id+'" />');
					$('#file_'+data.id).append('<span>'+data.fileName+'</span>');
					$('#file_'+data.id).append('&nbsp;');
					$('#file_'+data.id).append('<a href="javascript:void(0)" style="color: green;" onclick="removeFile(\''+data.id+'\');">删除</a>');
					$('#file_'+data.id).append('&nbsp;&nbsp;');
					$('#file_'+data.id).append('</div>');
					$('#knowtitleId').val(data.fileName);
				} else {
					alert(data.message);
				}
			},
			afterError : function(str) {
				alert('自定义错误信息: ' + str);
			}
		});
		uploadbutton.fileBox.change(function(e) {
			uploadbutton.submit();
		});
	});
</script>
<script type="text/javascript">
	var editor = null;
	$(function() {
		$('#btn_upload').bind('click', function() {
			$('.ke-button-common').click();
		});
		editor = KindEditor.create('textarea[id="contentId"]', {
			resizeType : 1,
			afterChange : function() {

			},
			cssPath : '<PF:basePath/>text/lib/kindeditor/editInner.css',
			uploadJson : basePath + 'actionImg/PubFPupload.do',
			allowPreviewEmoticons : false,
			allowImageUpload : true,
			items : [ 'source', 'fullscreen', '|', 'fontsize', 'forecolor',
					'bold', 'italic', 'underline', 'removeformat', '|',
					'justifyleft', 'justifycenter', 'justifyright' ]
		});
		$('#openChooseTypeButtonId').bind('click', function() {
			$('#myModal').modal({
				keyboard : false
			})
		});
		$('select', '#knowSubmitFormId').each(function(i, obj) {
			var val = $(obj).attr('val');
			$(obj).val(val);
		});
		$('#knowSubmitButtonId')
				.bind(
						'click',
						function() {
							editor.sync();
							if (!validate('knowSubmitFormId')) {
								$('#errormessageShowboxId').text('信息录入有误，请检查！');
							} else {
								if ($('#contentId').val().length > 1000000) {
									$('#errormessageShowboxId')
											.text(
													'文档内容超长（'
															+ $('#contentId')
																	.val().length
															+ '>1000000)');
									return false;
								}
								//判断附件是否上传
								if ($("input[name='fileId']").length == 0) {
									$('#errormessageShowboxId').text('请上传文件');
									return false;
								}
								$('#errormessageShowboxId').text('');
								if (confirm("是否提交数据?")) {
									$('#knowSubmitFormId').submit();
									$('#knowSubmitButtonId').hide();
									$('#knowSubmitButtonId')
											.before(
													"<h2><span class='label label-info glyphicon glyphicon-link'>提交中...</span></h2>");
								}
							}
						});
		validateInput('knowtypeTitleId', function(id, val, obj) {
			// 分类
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		//绑定一个表单的验证事件
		validateInput('knowtitleId', function(id, val, obj) {
			// 标题
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (valid_maxLength(val, 128)) {
				return {
					valid : false,
					msg : '长度不能大于' + 128
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		//绑定一个表单的验证事件
		validateInput('knowtagId', function(id, val, obj) {
			// 学生姓名
			if (valid_maxLength(val, 128)) {
				return {
					valid : false,
					msg : '长度不能大于' + 128
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		//编辑权限
		validateInput('writetypeId', function(id, val, obj) {
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (val == '2' && $('#docgroupId').val() == '0') {
				return {
					valid : false,
					msg : '请选择工作小组'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		//阅读权限
		validateInput('readtypeId', function(id, val, obj) {
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (val == '2' && $('#docgroupId').val() == '0') {
				return {
					valid : false,
					msg : '请选择工作小组'
				};
			}
			if ($('#docgroupId').val() != '0') {
				if (val == '0') {
					return {
						valid : false,
						msg : '阅读权限至少是小组'
					};
				}
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		//工作小组
		validateInput('docgroupId', function(id, val, obj) {
			return {
				valid : true,
				msg : '正确'
			};
		});
		$('a', '.showLableType').bind('click', function() {
			$('#knowtypeId').val($(this).attr('id'));
			$('#knowtypeTitleId').val($(this).text());
			$('#myModal').modal('hide');
		});
	});
	
	function removeFile(fileId){
		$("#file_"+fileId).remove();
	}
</script>
</html>