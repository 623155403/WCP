<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!-- ID,TITLE,DOCDESCRIBE,AUTHOR,PUBTIME,TAGKEY ,IMGID,VISITNUM,PRAISEYES,PRAISENO,HOTNUM,TYPENAME -->
<div class="row column_box">
	<div class="col-sm-12">
		<span class="glyphicon glyphicon-star  column_title">推荐阅读</span>
	</div>
</div>
<c:forEach var="topDoc" items="${topDocList}">
	<div class="row">
		<div class="col-sm-12 doc_node">
			<div class="media">
				<c:if test="${topDoc.imgurl!=null}">
					<a class="pull-right " href="webdoc/view/Pub${topDoc.docid}.html"
						style="max-height: 100px; max-width: 250px;"><img
						class="doc_max_img" src="${topDoc.imgurl}"><br> <span
						class="side_unit_info"></span> </a>
				</c:if>
				<div class="media-body">
					<a class="doc_node_max_title" style="font-size: 16px;"
						href="webdoc/view/Pub${topDoc.docid}.html">${topDoc.title}</a>
					<div class="doc_node_info">阅读(${topDoc.visitnum }) |
						评论(${topDoc.answeringnum })</div>
					<div class="doc_node_content">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						${topDoc.text } &nbsp;</div>
				</div>
			</div>
		</div>
	</div>
</c:forEach>