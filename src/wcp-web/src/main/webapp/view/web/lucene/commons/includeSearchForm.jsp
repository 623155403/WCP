<%@page import="java.net.URLEncoder"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="row">
	<div class="col-sm-12 ">
		<form action="websearch/PubDo.do" method="post">
			<div class="input-group">
				<input type="text" class="form-control" name="word" value="${word}"
					placeholder="请输入关键字..."> <span class="input-group-btn">
					<button class="btn btn-info" type="submit">检索</button>
				</span>
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 ">
		<c:forEach items="${hotCase}" var="node">
			<a href="websearch/PubDo.do?word=<PF:urlEncode val="${node}"/>">
				<span class="label label-danger" style="cursor: pointer;"
				onclick="javascript:luceneSearch('java')">${node}</span>
			</a>
		</c:forEach>
	</div>
</div>
