<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>检索-<PF:ParameterValue key="config.sys.title" /></title>
<jsp:include page="/view/conf/include-web.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox">
		<div class="container " style="background-color: #fff;">
			<div class="row column_box">
				<div class="col-lg-3"></div>
				<div class="col-lg-6">
					<div class="row">
						<div class="col-sm-12 column_box  hidden-xs">
							<img class="img-thumbnail center-block" src="text/img/logo.png" />
						</div>
						<div class="col-sm-12 column_box">
							<jsp:include page="commons/includeSearchForm.jsp"></jsp:include>
						</div>
					</div>
				</div>
				<div class="col-lg-3"></div>
			</div>
			<hr class="hr_split" style="margin-top: 50px;" />
			<!-- /.row -->
			<div class="row  hidden-xs">
				<div class="col-sm-7">
					<jsp:include page="commons/includeSearchTopDoc.jsp"></jsp:include>
				</div>
				<div class="col-sm-1"></div>
				<div class="col-sm-4">
					<jsp:include page="commons/includeSearchHotDocs.jsp"></jsp:include>
				</div>
			</div>
			<hr class="hr_split  hidden-xs" />
			<div class="row  hidden-xs">
				<div class="col-sm-12">
					<jsp:include page="commons/includeSearchTypes.jsp"></jsp:include>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>