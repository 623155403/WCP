<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>分类知识-<PF:ParameterValue key="config.sys.title" />
</title>
<jsp:include page="/view/conf/include-web.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox">
		<div class="container ">
			<div class="row">
				<div class="col-md-3 hidden-xs">
					<jsp:include page="commons/includePubType.jsp"></jsp:include>
				</div>
				<div class="col-md-9">
					<div class="row column_box">
						<div class="col-sm-12">
							<span class="glyphicon glyphicon-fire  column_title">分类<c:if
									test="${type!=null}">:${type.name}</c:if>
							</span><em>某些被赋予权限的知识在登录后被显示</em>
						</div>
					</div>
					<c:if test="${type!=null}"><jsp:include
							page="../operation/includeCreatOperate.jsp"></jsp:include>
					</c:if>
					<jsp:include page="commons/includePubTypeByBox.jsp"></jsp:include>
					<jsp:include page="commons/includeTypeDocs.jsp"></jsp:include>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>