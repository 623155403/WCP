<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${user.name}-<PF:ParameterValue key="config.sys.title" /></title>
<jsp:include page="/view/conf/include-web.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox">
		<div class="container ">
			<div class="row  column_box">
				<div class="col-md-12">
					<jsp:include page="../statis/commons/includeMyStatis.jsp"></jsp:include>
				</div>
			</div>
			<div class="row ">
				<div class="col-md-12">
					<div class="panel panel-default "
						style="background-color: #FCFCFA;">
						<div class="panel-body">
							<p>
								<jsp:include page="../operation/includeUserOperate.jsp"></jsp:include>
							</p>
							<div class="table-responsive">
								<hr style="margin: 8px;" />
								<c:if test="${type=='know'}">
									<jsp:include page="commons/includeUserKnows.jsp"></jsp:include>
								</c:if>
								<c:if test="${type=='file'}">
									<jsp:include page="commons/includeUserKnows.jsp"></jsp:include>
								</c:if>
								<c:if test="${type=='joy'}">
									<jsp:include page="commons/includeUserKnows.jsp"></jsp:include>
								</c:if>
								<c:if test="${type=='group'}">
									<jsp:include page="commons/includeUserGroups.jsp"></jsp:include>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	function knowBoxGoPage(page) {
		window.location = "webuser/PubHome.do?userid=${id}&query.currentPage="
				+ page;
	}
</script>
</html>