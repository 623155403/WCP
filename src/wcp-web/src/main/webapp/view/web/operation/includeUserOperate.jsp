<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="btn-group" role="group" aria-label="...">
	<c:if test="${type=='know'}">
		<a href="webuser/PubHome.do?type=know&userid=${userid}" class="btn btn-info">发布知识</a>
	</c:if>
	<c:if test="${type!='know'}">
		<a href="webuser/PubHome.do?type=know&userid=${userid}" class="btn btn-default">发布知识</a>
	</c:if>
	<c:if test="${type=='file'}">
		<a href="webuser/PubHome.do?type=file&userid=${userid}" class="btn btn-info">发布资源</a>
	</c:if>
	<c:if test="${type!='file'}">
		<a href="webuser/PubHome.do?type=file&userid=${userid}" class="btn btn-default">发布资源</a>
	</c:if>
	<c:if test="${type=='joy'}">
		<c:if test="${self}">
			<a href="webuser/PubHome.do?type=joy&userid=${userid}" class="btn btn-info">收藏夹</a>
		</c:if>
	</c:if>
	<c:if test="${type!='joy'}">
		<c:if test="${self}">
			<a href="webuser/PubHome.do?type=joy&userid=${userid}"  class="btn btn-default">收藏夹</a>
		</c:if>
	</c:if>
	<c:if test="${type=='group'}">
		<a href="webuser/PubHome.do?type=group&userid=${userid}"  class="btn btn-info">小组</a>
	</c:if>
	<c:if test="${type!='group'}">
		<a href="webuser/PubHome.do?type=group&userid=${userid}"  class="btn btn-default">小组</a>
	</c:if>
</div>
<c:if test="${self}">
	<div class="btn-group pull-right" role="group" aria-label="...">
		<button type="button" class="btn btn-primary" onclick="toUserEdit()">修改用户信息</button>
		<button type="button" class="btn btn-primary"
			onclick="toUserPwdEdit()">修改登录密码</button>
	</div>
	<script type="text/javascript">
		function toUserEdit() {
			window.location.href = "webuser/edit.do";
		}
		function toUserPwdEdit() {
			window.location.href = "webuser/editCurrentUserPwd.do";
		}
	</script>
</c:if>
