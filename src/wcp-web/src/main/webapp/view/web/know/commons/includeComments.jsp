<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fun"%>
<div class="row column_box">
	<div class="col-sm-12">
		<span class="glyphicon glyphicon-star  column_title">最新评论 </span>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<c:forEach items="${result.resultList}" var="node">
			<div class="col-sm-12">
				<hr class="hr_split" />
			</div>
			<div class="col-sm-12 doc_node">
				<div class="media">
					<a class="pull-left"
						href="webuser/PubHome.do?userid=${node.CUSER }"
						style="max-width: 200px; text-align: center;" title="王东">
						<img class="img-circle side_unit_author_img_min"
							src="<PF:basePath/>${node.IMGURL}">
						<br />
					</a>
					<div class="media-body">
						<div style="margin-left: 4px;" class="pull-left">
							<div class="side_unit_info">
								<span class="side_unit_info">
									<a>${node.CUSERNAME }</a>
									${node.CTIME }
								</span>
							</div>
							<div class="doc_node_content" style="margin: 4px;">${node.CONTENT}</div>
							<%-- 
							<div class="side_unit_info">
								<a>
									<span class="side_unit_info glyphicon glyphicon-thumbs-up">(20)</span>
								</a>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<a>
									<span class="side_unit_info glyphicon glyphicon-thumbs-down">(20)</span>
								</a>
							</div> --%>
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</div>
<c:if test="${fun:length(result.resultList) > 0}">
	<div class="row">
		<div class="col-xs-12">
			<ul class="pagination pagination-sm">
				<c:forEach var="page" begin="${result.startPage}"
					end="${result.endPage}" step="1">
					<c:if test="${page==result.currentPage}">
						<li class="active"><a>${page} </a></li>
					</c:if>
					<c:if test="${page!=result.currentPage}">
						<li><a href="webtype/view${typeid}/Pub${page}.html?pagenum=">${page}</a></li>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	</div>
</c:if>