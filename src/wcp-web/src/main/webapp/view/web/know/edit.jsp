<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmdoc.tld" prefix="DOC"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>编辑知识-<PF:ParameterValue key="config.sys.title" /></title>
<jsp:include page="/view/conf/include-web.jsp"></jsp:include>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/themes/default/default.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="<PF:basePath/>text/lib/kindeditor/zh_CN.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox">
		<div class="container">
			<div class="row column_box ">
				<div class="col-md-3  visible-lg visible-md">
					<jsp:include page="commons/includeNavigationDocEdit.jsp"></jsp:include>
				</div>
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li class="active">WCP</li>
								<li class="active">编辑知识</li>
							</ol>
						</div>
					</div>
					<form role="form" action="know/editsubmit.do" id="knowSubmitFormId"
						method="post">
						<input type="hidden" name="docid" value="${doce.doc.id}" />
						<DOC:canNoDelIsShow docId="${doce.doc.id}">
							<div class="row">
								<div class="col-md-12">
									<span
										style="color: #008000; font-size: 12px; font-weight: lighter;">作者:<span
										style="color: #D9534F; font-size: 14px; font-weight: bold;">${doce.doc.author}</span>于<PF:FormatTime
											date="${doce.doc.pubtime}" yyyyMMddHHmmss="yyyy年MM月dd日" />发布
									</span>
									<h1>
										<a href="index/FPDocShow.htm?id=${doc.id}">
											${doce.doc.title}</a> <small>[编辑:<c:if
												test="${doce.doc.writepop==1}">
									公开
									</c:if> <c:if test="${doce.doc.writepop==0}">
									私有
									</c:if> <c:if test="${doce.doc.writepop==2}">
									小组
									</c:if> ][读:<c:if test="${doce.doc.readpop==1}">
									公开
									</c:if> <c:if test="${doce.doc.readpop==0}">
									私有
									</c:if> <c:if test="${doce.doc.readpop==2}">
									小组
									</c:if> ]
										</small>
									</h1>
									<h1>
										<span class="typetagsearch" style="cursor: pointer;"
											title="${doce.type.name}"> <small>${doce.type.name}</small>
										</span>
									</h1>
								</div>
							</div>
						</DOC:canNoDelIsShow>
						<DOC:canDelIsShow docId="${doce.doc.id}">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="exampleInputEmail1"> 标题 <span
											class="alertMsgClass">*</span>
										</label> <input type="text" class="form-control" name="knowtitle"
											value="${doce.doc.title}" id="knowtitleId"
											placeholder="输入知识标题" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="exampleInputEmail1"> 文档分类 <span
											class="alertMsgClass">*</span>
										</label>
										<div class="row">
											<div class="col-md-9">
												<input type="text" class="form-control" id="knowtypeTitleId"
													readonly="readonly" placeholder="选择栏目分类"
													value="${doce.type.name}" /> <input type="hidden"
													name="knowtype" id="knowtypeId" value="${doce.type.id}" />
											</div>
											<div class="col-md-3">
												<button class="btn btn-info btn-sm" data-toggle="modal"
													id="openChooseTypeButtonId" data-target="#myModal">
													选择</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</DOC:canDelIsShow>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="exampleInputEmail1"> tag </label> <input
										type="text" class="form-control" id="knowtagId"
										value="${doce.doc.tagkey}" name="knowtag"
										placeholder="输入类别标签(如果没有系统将自动创建)" />
								</div>
							</div>
						</div>
						<div class="row">
							<DOC:canDelIsShow docId="${doce.doc.id}">
								<c:if test="${doce.doc.docgroupid==null}">
									<div class="col-md-4">
										<div class="form-group">
											<label for="exampleInputEmail1"> 是否发布到小组 </label> <select
												class="form-control" name="docgroup" id="docgroupId"
												val="${doce.doc.docgroupid}">
												<option value="0">否</option>
												<DOC:docGroupOption aroundS="[工作小组]:" />
											</select>
										</div>
									</div>
								</c:if>
							</DOC:canDelIsShow>
							<c:if test="${doce.doc.docgroupid!=null}">
								<input type="hidden" name="docgroupId"
									value="${doce.doc.docgroupid}" />
							</c:if>
							<DOC:canDelIsShow docId="${doce.doc.id}">
								<div class="col-md-4">
									<div class="form-group">
										<label for="exampleInputEmail1"> 编辑权限 <span
											class="alertMsgClass">*</span>
										</label> <select class="form-control" name="writetype"
											id="writetypeId" val="${doce.doc.writepop}">
											<option value="">~请选择~</option>
											<option value="0">创建人</option>
											<option value="1">公开</option>
											<option value="2">小组</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="exampleInputEmail1"> 阅读权限 <span
											class="alertMsgClass">*</span>
										</label> <select class="form-control" name="readtype" id="readtypeId"
											val="${doce.doc.readpop}">
											<option value="">~请选择~</option>
											<option value="0">创建人</option>
											<option value="1">公开</option>
											<option value="2">小组</option>
										</select>
									</div>
								</div>
							</DOC:canDelIsShow>
						</div>
						<div class="row">
							<div class="col-md-12">
								<textarea name="text" id="contentId"
									style="height: 500px; width: 100%;">${doce.texts.text1}</textarea>
							</div>
						</div>
						<!-- 修改 -->
						<c:if test="${doce.doc!=null&&doce.doc.id!=null}">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="exampleInputEmail1"> 修改原因 <span
											class="alertMsgClass">*</span>
										</label> <input type="text" class="form-control" id="knowEditNoteId"
											name="editNote" placeholder="修改原因将被记录在版本备注中" />
									</div>
								</div>
							</div>
							<script type="text/javascript">
								$(function() {
									validateInput('knowEditNoteId', function(
											id, val, obj) {
										// 分类
										if (valid_isNull(val)) {
											return {
												valid : false,
												msg : '不能为空'
											};
										}
										return {
											valid : true,
											msg : '正确'
										};
									});
								});
							</script>
						</c:if>
						<div class="row column_box">
							<div class="col-md-2">
								<button type="button" id="knowSubmitButtonId"
									class="btn btn-info">提交</button>
							</div>
							<div class="col-md-10">
								<span class="alertMsgClass" id="errormessageShowboxId"></span>
							</div>
						</div>
						<div class="row column_box">
							<div class="col-md-12"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择分类</h4>
				</div>
				<div class="modal-body" id="treeChooseTypeBoxId">
					<div class="modal-body" id="treeChooseTypeBoxId">
						<div class="row column_box">
							<div class="col-sm-12">
								<span class="glyphicon glyphicon-fire  column_title">知识分类</span>
							</div>
						</div>
						<div class="panel panel-default  side_column">
							<div class="panel-body">
								<ul class="doctypeUl">
									<c:forEach var="node" items="${types}">
										<c:if test="${node.parentid=='NONE'}">
											<li>
												<h5 class="showLableType">
													<a id="${node.id}">${node.name}</a>
													<c:if test="${node.num>0}">
														<span style="color: #D9534F; font-weight: bold;">${node.num}</span>
													</c:if>
												</h5>
												<ul>
													<c:forEach var="node1" items="${types}">
														<c:if test="${node1.parentid==node.id}">
															<li>
																<h5 class="showLableType">
																	<a id="${node1.id}">${node1.name}</a>
																	<c:if test="${node1.num>0}">
																		<span style="color: #D9534F; font-weight: bold;">${node1.num}</span>
																	</c:if>
																</h5>
																<ul class="list-inline">
																	<c:forEach var="node2" items="${types}">
																		<c:if test="${node2.parentid==node1.id}">
																			<li>
																				<h5 class="showLableType">
																					<span style="color: #5CB85C;"><a
																						id="${node2.id}"> ${node2.name}</a></span>
																					<c:if test="${node2.num>0}">
																						<span style="color: #D9534F; font-weight: bold;">${node2.num}</span>
																					</c:if>
																				</h5>
																			</li>
																		</c:if>
																	</c:forEach>
																</ul>
															</li>
														</c:if>
													</c:forEach>
												</ul>
											</li>
										</c:if>
									</c:forEach>
								</ul>
							</div>
						</div>
						<script type="text/javascript">
							$(function() {
								$('a', '.showLableType').bind(
										'click',
										function() {
											$('#knowtypeId').val(
													$(this).attr('id'));
											$('#knowtypeTitleId').val(
													$(this).text());
											$('#myModal').modal('hide');
										});
							});
						</script>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						关闭</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	var editor = null;

	$(function() {
		editor = KindEditor.create('textarea[id="contentId"]', {
			resizeType : 1,
			afterChange : function() {
				//生成导航目录
				initLeftMenuFromHtml(this.html());
			},
			cssPath : '<PF:basePath/>text/lib/kindeditor/editInner.css',
			uploadJson : basePath + 'actionImg/PubFPupload.do',
			formatUploadUrl : false,
			allowPreviewEmoticons : false,
			allowImageUpload : true,
			items : [ 'source', 'fullscreen', '|', 'fontsize', 'forecolor',
					'bold', 'italic', 'underline', 'removeformat', '|',
					'justifyleft', 'justifycenter', 'justifyright',
					'insertorderedlist', 'insertunorderedlist', 'lineheight',
					'|', 'formatblock', 'quickformat', 'table', 'hr',
					'pagebreak', '|', 'link', 'image', 'code', 'insertfile',
					'wcpknow' ]
		});
		$('#openChooseTypeButtonId').bind('click', function() {
			$('#myModal').modal({
				keyboard : false
			})
		});
		$('select', '#knowSubmitFormId').each(function(i, obj) {
			var val = $(obj).attr('val');
			$(obj).val(val);
		});
		$('#knowSubmitButtonId')
				.bind(
						'click',
						function() {
							editor.sync();
							if (!validate('knowSubmitFormId')) {
								$('#errormessageShowboxId').text('信息录入有误，请检查！');
							} else {
								if ($('#contentId').val() == null
										|| $('#contentId').val() == '') {
									$('#errormessageShowboxId')
											.text('请录入文档内容！');
									return false;
								}
								if ($('#contentId').val().length > 1000000) {
									$('#errormessageShowboxId')
											.text(
													'文档内容超长（'
															+ $('#contentId')
																	.val().length
															+ '>1000000)');
									return false;
								}
								if (confirm("是否提交数据?")) {
									$('#knowSubmitFormId').submit();
									$('#knowSubmitButtonId').hide();
									$('#knowSubmitButtonId')
											.before(
													"<h2><span class='label label-info glyphicon glyphicon-link'>提交中...</span></h2>");
								}
							}
						});
		validateInput('knowtypeTitleId', function(id, val, obj) {
			// 分类
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		//绑定一个表单的验证事件
		validateInput('knowtitleId', function(id, val, obj) {
			// 标题
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (valid_maxLength(val, 128)) {
				return {
					valid : false,
					msg : '长度不能大于' + 128
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		//绑定一个表单的验证事件
		validateInput('knowtagId', function(id, val, obj) {
			// 学生姓名
			if (valid_maxLength(val, 128)) {
				return {
					valid : false,
					msg : '长度不能大于' + 128
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		//编辑权限
		validateInput('writetypeId', function(id, val, obj) {
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (val == '2' && $('#docgroupId').val() == '0') {
				return {
					valid : false,
					msg : '请选择工作小组'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		//阅读权限
		validateInput('readtypeId', function(id, val, obj) {
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (val == '2' && $('#docgroupId').val() == '0') {
				return {
					valid : false,
					msg : '请选择工作小组'
				};
			}
			if ($('#docgroupId').val() != '0') {
				if (val == '0') {
					return {
						valid : false,
						msg : '阅读权限至少是小组'
					};
				}
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		//工作小组
		validateInput('docgroupId', function(id, val, obj) {
			return {
				valid : true,
				msg : '正确'
			};
		});
	});
	function clickDocType(id, text) {
		$('#knowtypeTitleId').val(text);
		$('#knowtypeId').val(id);
		$('#myModal').modal('hide');
	}
</script>
</html>