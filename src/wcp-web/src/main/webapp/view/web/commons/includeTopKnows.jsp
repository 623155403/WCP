<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!-- ID,TITLE,DOCDESCRIBE,AUTHOR,PUBTIME,TAGKEY ,IMGID,VISITNUM,PRAISEYES,PRAISENO,HOTNUM,TYPENAME -->
<div class="row column_box">
	<div class="col-sm-12">
		<span class="glyphicon glyphicon-star  column_title">推荐阅读</span>
	</div>
</div>
<c:forEach var="topDoc" items="${topDocList}">
	<div class="row">
		<div class="col-sm-12">
			<hr class="hr_split" />
		</div>
		<div class="col-sm-12 doc_node">
			<div class="doc_node_max_title">
				<a class="doc_node_max_title"
					href="webdoc/view/Pub${topDoc.docid}.html">${topDoc.title}</a>
			</div>
			<c:if test="${topDoc.imgurl!=null}">
				<div class="doc_max_img_box">
					<img class="doc_max_img" src="${topDoc.imgurl}">
				</div>
				<br />
			</c:if>
			<div class="doc_node_info">阅读(${topDoc.visitnum }) |
				评论(${topDoc.answeringnum })</div>
			<div class="doc_node_content">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				${topDoc.text } &nbsp;</div>
		</div>
	</div>
</c:forEach>