<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="super_content">
	<c:if test="${MESSAGE!=null}">
		<div style="color: red;">${MESSAGE}</div>
	</c:if>
</div>