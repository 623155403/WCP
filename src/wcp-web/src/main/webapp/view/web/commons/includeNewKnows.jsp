<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="row column_box">
	<div class="col-sm-12">
		<span class="glyphicon glyphicon-star  column_title">最新知识 </span>
	</div>
</div>
<div class="row">
	<c:forEach items="${newdocs}" var="node">
		<div class="col-sm-12">
			<hr class="hr_split" />
		</div>
		<div class="col-sm-12 doc_node">
			<div class="media">
				<a class="pull-right"
					href="webuser/PubHome.do?userid=${node.userid }"
					style="max-width: 200px; text-align: center;"
					title="${node.title }"> <img
					class="img-rounded side_unit_author_img"
					src="<PF:basePath/>${node.photourl }"> <br /> <span
					class="side_unit_info">${node.username}</span>
				</a>
				<div class="media-body">
					<div style="margin-left: 4px;" class="pull-left">
						<div class="doc_node_title_box">
							<a class="doc_node_title"
								href="webdoc/view/Pub${node.docid}.html">${node.title} <c:if
									test="${node.domtype=='3'}">
									<span class="glyphicon glyphicon-home"></span>
								</c:if> <c:if test="${node.domtype=='4'}">
									<span class="glyphicon glyphicon-bookmark"></span>
								</c:if> <c:if test="${node.domtype=='1'}">
									<span class="glyphicon glyphicon-file"></span>
								</c:if> <c:if test="${node.domtype=='5'}">
									<span style="color: #d9534f;"
										class="glyphicon glyphicon-download-alt"></span>
								</c:if>
							</a>
						</div>
						<c:if test="${node.text!=null&&node.text!=''}">
							<div class="doc_node_content_box" >
								<p class="doc_node_content">${node.text}</p>
							</div>
						</c:if>
						<div class="side_unit_info side_unit_info_left_box">${node.typename}
							阅读(${node.visitnum}) 评论(${node.answeringnum})${node.pubtime}</div>
					</div>
				</div>
			</div>
		</div>
	</c:forEach>
</div>