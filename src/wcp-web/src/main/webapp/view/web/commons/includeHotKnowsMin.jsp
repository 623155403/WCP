<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="row column_box">
	<div class="col-sm-12">
		<span class="glyphicon glyphicon-fire  column_title">常用知识</span>
	</div>
</div>
<div class="row side_column">
	<div class="col-sm-12">
		<div class="list-group">
			<c:forEach items="${hotdocs}" varStatus="status" var="node">
				<a href="webdoc/view/Pub${node.docid}.html" class="list-group-item">
					<div class="side_unit_title">${node.title}</div>
					<div class="side_unit_info">
						<c:if test="${node.domtype=='3'}">
							<span class="glyphicon glyphicon-home"></span>
						</c:if>
						<c:if test="${node.domtype=='1'}">
							<span class="glyphicon glyphicon-file"></span>
						</c:if>
						<c:if test="${node.domtype=='4'}">
							<span class="glyphicon glyphicon-bookmark"></span>
						</c:if>
						<c:if test="${node.domtype=='5'}">
							<span style="color: #d9534f;"
								class="glyphicon glyphicon-download-alt"></span>
						</c:if>
						阅读(${node.visitnum})评论(${node.answeringnum})
					</div>
				</a>
			</c:forEach>
		</div>
	</div>
</div>