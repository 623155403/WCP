<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="super_content  hidden-xs" style="margin: 0px;">
	<div class="container ">
		<div class="row">
			<div class="col-sm-4"><jsp:include
					page="../commons/includeTowDCode.jsp"></jsp:include></div>
			<div class="col-sm-8 putServerBox">
				<h1>推荐服务</h1>
				<ul id="recommendServiceList">
					<c:if test="${USEROBJ!=null}">
						<li>
							<a href="frame/index.do">管理控制台</a>
						</li>
					</c:if>
				</ul>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	//页面加载完毕，执行如下方法。
	$(function(){
		$.post("home/PubrecommendServiceList.do", {}, function(data) {
			for(i in data){
				var url = '<li><a href="http://'+data[i].url+'" target="_blank">'+data[i].webname+'</a></li>'; 
				$("#recommendServiceList").append(url);
			}
		}, "json");
	});
</script>