package com.farm.web.init;

import javax.servlet.ServletContext;

import com.farm.parameter.service.impl.ConstantVarService;
import com.farm.parameter.service.impl.PropertiesFileService;
import com.farm.web.constant.FarmConstant;
import com.farm.web.task.ServletInitJobInter;

public class InitParameter implements ServletInitJobInter {

	@Override
	public void execute(ServletContext context) {
		// 注册常量
		ConstantVarService.registConstant("farm.constant.session.key.logintime.", FarmConstant.SESSION_LOGINTIME);
		ConstantVarService.registConstant("farm.constant.session.key.current.org", FarmConstant.SESSION_ORG);
		ConstantVarService.registConstant("farm.constant.session.key.current.roles", FarmConstant.SESSION_ROLES);
		ConstantVarService.registConstant("farm.constant.session.key.current.actions", FarmConstant.SESSION_USERACTION);
		ConstantVarService.registConstant("farm.constant.session.key.current.menu", FarmConstant.SESSION_USERMENU);
		ConstantVarService.registConstant("farm.constant.session.key.current.user", FarmConstant.SESSION_USEROBJ);
		ConstantVarService.registConstant("farm.constant.session.key.current.userphoto",FarmConstant.SESSION_USERPHOTO);
		ConstantVarService.registConstant("farm.constant.session.key.go.url", FarmConstant.SESSION_GO_URL);
		ConstantVarService.registConstant("farm.constant.session.key.from.url", FarmConstant.SESSION_FROM_URL);
		ConstantVarService.registConstant("farm.constant.app.treecodelen",String.valueOf(FarmConstant.MENU_TREECODE_UNIT_LENGTH));
		ConstantVarService.registConstant("farm.constant.webroot.path", context.getRealPath(""));
		// 注册配置文件
		PropertiesFileService.registConstant("config");
		PropertiesFileService.registConstant("indexConfig");
		PropertiesFileService.registConstant("jdbc");
		PropertiesFileService.registConstant("document");
		PropertiesFileService.registConstant("cache");
		PropertiesFileService.registConstant("webapp");
		PropertiesFileService.registConstant("i18n");
		// 注册lucene索引目录的常量
		ConstantVarService.registConstant("LUCENE_DIR_KNOW", "know");
		ConstantVarService.registConstant("LUCENE_DIR_SITE", "site");
		ConstantVarService.registConstant("LUCENE_DIR_FILE", "file");
	}

}
