package com.farm.wcp.controller;

import java.sql.SQLException;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.authority.domain.User;
import com.farm.authority.service.UserServiceInter;
import com.farm.core.page.ViewMode;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.doc.server.FarmDocManagerInter;
import com.farm.doc.server.FarmDocOperateRightInter;
import com.farm.doc.server.FarmDocRunInfoInter;
import com.farm.doc.server.FarmDocgroupManagerInter;
import com.farm.doc.server.FarmDocmessageManagerInter;
import com.farm.doc.server.FarmFileManagerInter;
import com.farm.parameter.FarmParameterService;
import com.farm.util.validate.ValidUtils;
import com.farm.wcp.know.service.KnowServiceInter;
import com.farm.web.WebUtils;

/**
 * 统计
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/webuser")
@Controller
public class UserController extends WebUtils {
	@Resource
	private FarmDocgroupManagerInter farmDocgroupManagerImpl;
	@Resource
	private FarmFileManagerInter farmFileManagerImpl;
	@Resource
	private FarmDocManagerInter farmDocManagerImpl;
	@Resource
	private FarmDocRunInfoInter farmDocRunInfoImpl;
	@Resource
	private KnowServiceInter knowServiceImpl;
	@Resource
	private FarmDocmessageManagerInter farmDocmessageManagerImpl;
	@Resource
	private FarmDocOperateRightInter farmDocOperateRightImpl;
	@Resource
	private UserServiceInter userServiceImpl;

	private final static Logger log = Logger.getLogger(UserController.class);

	/**
	 * 查看用户信息
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/info")
	public ModelAndView userInfo(HttpSession session) {
		try {
			User user = userServiceImpl.getUserEntity(getCurrentUser(session).getId());
			if (!ValidUtils.isEmptyString(user.getImgid())) {
				String photourl = farmFileManagerImpl.getFileURL(user.getImgid());
			}
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.toString()).returnModelAndView("");
		}
		return ViewMode.getInstance().returnModelAndView("");
	}

	/**
	 * 修改用户信息
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/edit")
	public ModelAndView editUser(HttpSession session) {
		try {
			User user = userServiceImpl.getUserEntity(getCurrentUser(session).getId());
			String name = user.getName();
			String photoid = user.getImgid();
			String photourl = null;
			if (photoid != null && photoid.trim().length() > 0) {
				photourl = farmFileManagerImpl.getFileURL(photoid);
			}
			return ViewMode.getInstance().putAttr("user", user).putAttr("name", name).putAttr("photoid", photoid)
					.putAttr("photourl", photourl).returnModelAndView("web/user/userInfoEdit");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.toString()).returnModelAndView("web/user/userInfoEdit");
		}
	}

	/**
	 * 用户注册
	 * 
	 * @return
	 */
	@RequestMapping("/PubRegist")
	public ModelAndView regist(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("/web/user/regist");
	}

	/**
	 * 用户注册提交
	 * 
	 * @return
	 * @throws UserNoExistException
	 */
	@RequestMapping("/PubRegistCommit")
	public ModelAndView registSubmit(String photoid, String loginname, String name, String password,
			HttpSession session) {
		User user = new User();
		try {
			if (FarmParameterService.getInstance().getParameter("config.sys.registable ").equals("false")) {
				throw new RuntimeException("该操作已经被管理员禁用!");
			}
			user.setImgid(photoid);
			user.setLoginname(loginname);
			user.setName(name);
			user.setState("1");
			user.setType("1");
			user = userServiceImpl.registUser(user);
			userServiceImpl.editLoginPassword(loginname,
					FarmParameterService.getInstance().getParameter("config.default.password"), password);
		} catch (Exception e) {
			e.printStackTrace();
			return ViewMode.getInstance().returnRedirectUrl("/home/Pubindex.html");
		}
		return ViewMode.getInstance().returnRedirectUrl("/home/Pubindex.html");
	}

	/**
	 * 用户首页
	 * 
	 * @return
	 */
	@RequestMapping("/PubHome")
	public ModelAndView showUserHome(String userid, String type, Integer num, HttpSession session) {
		ViewMode mode = ViewMode.getInstance();
		boolean isSelf = false;
		DataResult result = null;
		User user = null;
		try {
			if (userid == null) {
				user = userServiceImpl.getUserEntity(getCurrentUser(session).getId());
				mode.putAttr("self", true);
				isSelf = true;
			} else {
				user = userServiceImpl.getUserEntity(userid);
				if (user.getId().equals(getCurrentUser(session) != null ? getCurrentUser(session).getId() : "none")) {
					mode.putAttr("self", true);
					isSelf = true;
				} else {
					mode.putAttr("self", false);
					isSelf = false;
				}
			}
			if (user != null) {
				DataResult users = farmDocRunInfoImpl.getStatUser(user);
				if (user.getImgid() != null && user.getImgid().trim().length() > 0) {
					mode.putAttr("photourl", farmFileManagerImpl.getFileURL(user.getImgid()));
				}
				mode.putAttr("users", users);
			}
			// --------------------------查询只是列表和小组------------------------------
			if (num == null) {
				num = 1;
			}
			if (type == null) {
				type = "know";
			}
			if (type.equals("know")) {
				// 发布知识
				result = isSelf ? farmDocRunInfoImpl.userDocs(user.getId(), "1", 10, num)
						: farmDocRunInfoImpl.userPubDocs(user.getId(), "1", 10, num);
			}
			if (type.equals("file")) {
				// 发布资源
				result = isSelf ? farmDocRunInfoImpl.userDocs(user.getId(), "5", 10, num)
						: farmDocRunInfoImpl.userPubDocs(user.getId(), "5", 10, num);
			}
			if (type.equals("joy")) {
				if (userid != null && !userid.isEmpty()) {
					// 收藏夹
					result = farmDocRunInfoImpl.getUserEnjoyDoc(user.getId()).setCurrentPage(num).setPagesize(10)
							.search();
					result.runformatTime("PUBTIME", "yyyy-MM-dd HH:mm");
				}
			}
			if (type.equals("group")) {
				// 加入小组
				result = farmDocgroupManagerImpl.getGroupsByUser(user.getId(), 16, num);
			}
		} catch (SQLException e) {
			return mode.setError(e.toString()).returnModelAndView("web/error");
		}
		return mode.putAttr("docs", result).putAttr("userid", user.getId()).putAttr("type", type)
				.returnModelAndView("web/user/userHome");
	}

	/**
	 * 更新当前登录用户信息
	 * 
	 * @param id
	 *            用户ID
	 * @param name
	 *            用户名称
	 * @param photoid
	 *            头像ID
	 * @return ModelAndView
	 */
	@RequestMapping("/editCurrentUser")
	public ModelAndView editCurrentUser(String name, String photoid, HttpSession session) {
		try {
			userServiceImpl.editCurrentUser(getCurrentUser(session).getId(), name, photoid);
		} catch (Exception e) {
			e.printStackTrace();
			return ViewMode.getInstance().setError(e.toString()).returnModelAndView("web/error");
		}
		return ViewMode.getInstance().returnRedirectUrl("/webuser/PubHome.do");
	}

	/**
	 * 跳转到修改密码页面 v1.0 zhanghc 2015年9月29日下午6:49:40
	 * 
	 * @param id
	 * @param name
	 * @param photoid
	 * @return ModelAndView
	 */
	@RequestMapping("/editCurrentUserPwd")
	public ModelAndView editCurrentUserPwd() {
		return ViewMode.getInstance().returnModelAndView("web/user/passwordEdit");
	}

	/**
	 * 更新当前登录用户的密码 v1.0 zhanghc 2015年9月29日下午6:54:53
	 * 
	 * @return ModelAndView
	 */
	@RequestMapping("/editCurrentUserPwdCommit")
	public ModelAndView editCurrentUserPwdCommit(String password, String newPassword, HttpSession session) {
		try {
			userServiceImpl.editCurrentUserPwdCommit(getCurrentUser(session).getId(), password, newPassword);
		} catch (Exception e) {
			e.printStackTrace();
			return ViewMode.getInstance().setError(e.toString()).returnModelAndView("web/error");
		}
		return ViewMode.getInstance().returnRedirectUrl("/webuser/PubHome.do");
	}

	/**
	 * 
	 * v1.0 zhanghc 2015年9月30日上午10:14:12 void
	 */
	@RequestMapping("/validCurrUserPwd")
	public Map<String, Object> validCurrUserPwd(String password, HttpSession session) {
		try {
			boolean b = userServiceImpl.validCurrentUserPwd(getCurrentUser(session).getId(), password);
			return ViewMode.getInstance().putAttr("validCurrentUserPwd", b).returnObjMode();
		} catch (Exception e) {
			e.printStackTrace();
			return ViewMode.getInstance().setError(e.toString()).returnObjMode();
		}
	}
}
