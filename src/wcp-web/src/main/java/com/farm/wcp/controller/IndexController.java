package com.farm.wcp.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.doc.domain.Weburl;
import com.farm.doc.domain.ex.DocBrief;
import com.farm.doc.domain.ex.GroupBrief;
import com.farm.doc.domain.ex.TypeBrief;
import com.farm.doc.server.FarmDocRunInfoInter;
import com.farm.doc.server.FarmDocTypeInter;
import com.farm.doc.server.FarmDocgroupManagerInter;
import com.farm.doc.server.FarmFileManagerInter;
import com.farm.doc.server.FarmtopServiceInter;
import com.farm.doc.server.WeburlServiceInter;
import com.farm.wcp.know.service.KnowServiceInter;
import com.farm.wcp.util.ZxingTowDCode;
import com.farm.web.WebUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

/**
 * 文件
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/home")
@Controller
public class IndexController extends WebUtils {
	@Resource
	private FarmDocRunInfoInter farmDocRunInfoImpl;
	@Resource
	private KnowServiceInter KnowServiceImpl;
	@Resource
	private FarmtopServiceInter farmTopServiceImpl;
	@Resource
	private FarmFileManagerInter farmFileManagerImpl;
	@Resource
	private FarmDocTypeInter farmDocTypeManagerImpl;
	@Resource
	private FarmDocgroupManagerInter farmDocgroupManagerImpl;
	@Resource
	private WeburlServiceInter weburlServiceImpl;

	/**
	 * 首页
	 * 
	 * @return
	 */
	@RequestMapping("/Pubindex")
	public ModelAndView index(HttpSession session) {
		// 获取前五条置顶文档
		List<DocBrief> topdocs = farmDocRunInfoImpl.getPubTopDoc(5);
		// 加载热词
		List<DocBrief> hotdocs = farmDocRunInfoImpl.getPubHotDoc(10);
		// 最新知识
		List<DocBrief> newdocs = farmDocRunInfoImpl.getNewKnowList(6);
		// 分类
		List<TypeBrief> typesons = farmDocTypeManagerImpl.getTypeInfos("NONE");
		// 获得最热小组
		List<GroupBrief> groups = farmDocgroupManagerImpl.getHotDocGroups(10,getCurrentUser(session));
		
		return ViewMode.getInstance()
				.putAttr("hotdocs", hotdocs)
				.putAttr("groups", groups)
				.putAttr("typesons", typesons)
				.putAttr("newdocs", newdocs)
				.putAttr("topDocList", topdocs)
				.returnModelAndView("web/index");
	}
	
	/**
	 * 展示二维码
	 */
	@RequestMapping("/PubQRCode")
	public void QRCode(HttpServletRequest request, HttpServletResponse response) {
		OutputStream outp = null;
		try {
			String path = request.getContextPath();
			String text = request.getScheme() + "://" + request.getServerName()
					+ ":" + request.getServerPort() + path + "/";
			int width = 300;
			int height = 300;
			// 二维码的图片格式
			String format = "gif";
			Hashtable hints = new Hashtable();
			// 内容所使用编码
			hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
			BitMatrix bitMatrix = new MultiFormatWriter().encode(text,
					BarcodeFormat.QR_CODE, width, height, hints);
			// 关于文件下载时采用文件流输出的方式处理：
			response.setContentType("application/x-download");
			String filedownload = "想办法找到要提供下载的文件的物理路径＋文件名";
			String filedisplay = "给用户提供的下载文件名";
			filedisplay = URLEncoder.encode(filedisplay, "UTF-8");
			response.addHeader("Content-Disposition", "attachment;filename="
					+ filedisplay);
			outp = response.getOutputStream();
			ZxingTowDCode.writeToStream(bitMatrix, format, outp);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (outp != null) {
				try {
					outp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 获取推荐服务
	 * v1.0 zhanghc 2015年10月10日下午6:44:27
	 * @return
	 * List<?>
	 */
	@RequestMapping("/PubrecommendServiceList")
	@ResponseBody
	public List<?> recommendServiceList() {
		List<Weburl> weburlList = weburlServiceImpl.getList();
		return ViewMode.returnListObjMode(weburlList);
	}
	
}
