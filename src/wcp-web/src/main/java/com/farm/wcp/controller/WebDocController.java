package com.farm.wcp.controller;


import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.doc.domain.FarmDoctype;
import com.farm.doc.domain.ex.DocEntire;
import com.farm.doc.server.FarmDocManagerInter;
import com.farm.doc.server.FarmDocRunInfoInter;
import com.farm.doc.server.FarmDocTypeInter;
import com.farm.doc.server.FarmDocgroupManagerInter;
import com.farm.doc.server.FarmFileManagerInter;
import com.farm.wcp.know.service.KnowServiceInter;
import com.farm.web.WebUtils;

/**
 * 网络文档
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/webdoc")
@Controller
public class WebDocController extends WebUtils {
	@Resource
	private FarmDocgroupManagerInter farmDocgroupManagerImpl;
	@Resource
	private FarmFileManagerInter farmFileManagerImpl;
	@Resource
	private FarmDocManagerInter farmDocManagerImpl;
	@Resource
	private FarmDocRunInfoInter farmDocRunInfoImpl;
	@Resource
	private KnowServiceInter KnowServiceImpl;
	@Resource
	private FarmDocTypeInter farmDocTypeManagerImpl;
	/**
	 * 下载网页并创建文档
	 * 
	 * @return
	 */
	@RequestMapping("/downWeb")
	public ModelAndView downWeb() {// TODO
		return ViewMode.getInstance().returnModelAndView("");
	}

	/**
	 * 提交网络文档URL
	 * 
	 * @return
	 */
	@RequestMapping("/downWebCommit")
	public ModelAndView downWebCommit(String url, String typeid, HttpSession session) {// TODO
		try {
			DocEntire doc = KnowServiceImpl.getDocByWeb(url, getCurrentUser(session));
			if (typeid != null && !typeid.toUpperCase().trim().equals("NONE")
					&& !typeid.toUpperCase().trim().equals("")) {
				FarmDoctype doctype = farmDocTypeManagerImpl.getType(typeid);
				doc.setType(doctype);
			}
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.toString()).returnModelAndView("");
		}
		return ViewMode.getInstance().returnModelAndView("");
	}
}
