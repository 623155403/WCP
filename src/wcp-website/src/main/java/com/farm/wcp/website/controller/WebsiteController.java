package com.farm.wcp.website.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.authority.service.UserServiceInter;
import com.farm.core.page.ViewMode;
import com.farm.doc.domain.Doc;
import com.farm.doc.domain.FarmDocfile;
import com.farm.doc.domain.FarmDoctype;
import com.farm.doc.domain.ex.DocEntire;
import com.farm.doc.exception.CanNoWriteException;
import com.farm.doc.server.FarmDocManagerInter;
import com.farm.doc.server.FarmDocOperateRightInter;
import com.farm.doc.server.FarmDocOperateRightInter.POP_TYPE;
import com.farm.doc.server.FarmDocRunInfoInter;
import com.farm.doc.server.FarmDocTypeInter;
import com.farm.doc.server.FarmDocgroupManagerInter;
import com.farm.doc.server.FarmDocmessageManagerInter;
import com.farm.doc.server.FarmFileManagerInter;
import com.farm.doc.server.exception.NotIsHttpZipException;
import com.farm.lucene.FarmLuceneFace;
import com.farm.lucene.server.DocIndexInter;
import com.farm.wcp.website.server.WcpWebSiteManagerInter;
import com.farm.wcp.website.util.HttpWebSite;
import com.farm.web.WebUtils;

/**
 * html静态站点
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/website")
@Controller
public class WebsiteController extends WebUtils {
	@Resource
	private FarmDocgroupManagerInter farmDocgroupManagerImpl;
	@Resource
	private FarmFileManagerInter farmFileManagerImpl;
	@Resource
	private FarmDocManagerInter farmDocManagerImpl;
	@Resource
	private FarmDocRunInfoInter farmDocRunInfoImpl;
	@Resource
	private FarmDocmessageManagerInter farmDocmessageManagerImpl;
	@Resource
	private FarmDocOperateRightInter farmDocOperateRightImpl;
	@Resource
	private UserServiceInter userServiceImpl;
	@Resource
	private WcpWebSiteManagerInter wcpWebSiteManagerImpl;
	@Resource
	private FarmDocTypeInter farmDocTypeManagerImpl;
	/**
	 * 请求创建站点页面
	 * 
	 */
	@RequestMapping("/add")
	public ModelAndView showPage(String typeid) {
		if (typeid != null && !typeid.toUpperCase().trim().equals("NONE") && !typeid.toUpperCase().trim().equals("")) {
			FarmDoctype doctype = farmDocTypeManagerImpl.getType(typeid);
			DocEntire doc = new DocEntire();
			doc.setType(doctype);
		}
		return ViewMode.getInstance().returnModelAndView("");
	}

	@RequestMapping("/edit")
	public ModelAndView showEditPage(String docid) {
		DocEntire doc = farmDocManagerImpl.getDoc(docid);
		docid = doc.getDoc().getId();
		String tag = doc.getDoc().getTagkey();
		String title = doc.getDoc().getTitle();
		String readtype = doc.getDoc().getReadpop();
		String writetype = doc.getDoc().getWritepop();
		String knowtype = doc.getType().getId();
		String text = doc.getTexts().getText1();
		List<FarmDocfile> files = farmFileManagerImpl.getAllTypeFileForDoc(docid, ".zip");
		if (files.size() > 0) {
			String zipfileId = files.get(0).getId();
		}
		return ViewMode.getInstance().returnModelAndView("");
	}

	/**
	 * 提交站点创建请求
	 * 
	 * @throws NotIsHttpZipException
	 * 
	 */
	@RequestMapping("/addCommit")
	public ModelAndView creatSiteCommit(String fileid, String title, String tag, String typeid, String readtype,
			String writetype, String text, String groupid, HttpSession session) throws NotIsHttpZipException {
		try {
			DocEntire doc = wcpWebSiteManagerImpl.creatWebSite(fileid, title, tag, typeid, POP_TYPE.getEnum(readtype),
					POP_TYPE.getEnum(writetype), text, groupid, getCurrentUser(session));
			String docid = doc.getDoc().getId();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.toString()).returnModelAndView("");
		}
		return ViewMode.getInstance().returnModelAndView("");
	}

	/**
	 * 提交站点更新请求
	 * 
	 * @throws CanNoWriteException
	 * @throws NotIsHttpZipException
	 * 
	 */
	@RequestMapping("/editCommit")
	public ModelAndView editSiteCommit(String docid, String fileid, String title, String tag, String typeid,
			String readtype, String writetype, String text, String groupid, String editNote, HttpSession session)
					throws NotIsHttpZipException, CanNoWriteException {
		try {
			if ("0".equals(groupid)) {
				groupid = null;
			}
			// 高级权限用户修改
			if (farmDocOperateRightImpl.isDel(getCurrentUser(session), farmDocManagerImpl.getDocOnlyBean(docid))) {
				DocEntire doc = wcpWebSiteManagerImpl.editSite(docid, fileid, title, tag, typeid,
						POP_TYPE.getEnum(readtype), POP_TYPE.getEnum(writetype), text, getCurrentUser(session), groupid,
						editNote);
				return ViewMode.getInstance().returnModelAndView("");
			}
			// 低级权限用户修改
			{
				DocEntire doc = wcpWebSiteManagerImpl.editSite(docid, fileid, tag, text, getCurrentUser(session),
						editNote);
				String id = doc.getDoc().getId();
			}
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.toString()).returnModelAndView("");
		}
		return ViewMode.getInstance().returnModelAndView("");
	}

	/**
	 * 访问站点
	 * 
	 * @throws Exception
	 * 
	 */
	@RequestMapping("/view")
	public ModelAndView visitSite(String docid, HttpSession session, HttpServletRequest request) throws Exception {
		try {
			DocEntire doc = wcpWebSiteManagerImpl.getWebSite(docid, getCurrentUser(session));
			farmDocRunInfoImpl.visitDoc(docid, getCurrentUser(session), getCurrentIp(request));
			String userId = "noLogin";
			if (getCurrentUser(session) != null) {
				userId = getCurrentUser(session).getId();
			}
			boolean isenjoy = farmDocRunInfoImpl.isEnjoyDoc(userId, docid);
			String typeid = doc.getType().getId();
			Set<String> fileTypes = new HashSet<String>();
			for (FarmDocfile node : doc.getFiles()) {
				fileTypes.add(node.getExname().trim().replace(".", "").toUpperCase());
			}
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.toString()).returnModelAndView("");
		}
		return ViewMode.getInstance().returnModelAndView("");
	}
}
