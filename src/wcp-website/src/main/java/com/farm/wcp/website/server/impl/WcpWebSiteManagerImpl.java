package com.farm.wcp.website.server.impl;

import java.io.File;
import java.io.IOException;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.farm.core.auth.domain.LoginUser;
import com.farm.doc.domain.Doc;
import com.farm.doc.domain.FarmDocfile;
import com.farm.doc.domain.ex.DocEntire;
import com.farm.doc.exception.CanNoWriteException;
import com.farm.doc.server.FarmDocManagerInter;
import com.farm.doc.server.FarmDocOperateRightInter;
import com.farm.doc.server.FarmDocTypeInter;
import com.farm.doc.server.FarmDocOperateRightInter.POP_TYPE;
import com.farm.doc.server.FarmFileManagerInter;
import com.farm.doc.server.exception.NotIsHttpZipException;
import com.farm.doc.util.DocTagUtils;
import com.farm.parameter.FarmParameterService;
import com.farm.wcp.website.server.WcpWebSiteManagerInter;
import com.farm.wcp.website.util.HttpWebSite;
import com.farm.wcp.website.util.ZipUtils;

@Service
public class WcpWebSiteManagerImpl implements WcpWebSiteManagerInter {
	@Resource
	private FarmDocManagerInter farmDocManagerImpl;
	@Resource
	private FarmFileManagerInter farmFileManagerImpl;
	@Resource
	private FarmDocOperateRightInter farmDocOperateRightImpl;
	@Resource
	private FarmDocTypeInter farmDocTypeManagerImpl;
	@Override
	@Transactional
	public HttpWebSite creatWebSite(String zipfileId, String title, String tags, String type, POP_TYPE POP_TYPE_read,
			POP_TYPE POP_TYPE_write, String describe, String groupId, LoginUser user) throws NotIsHttpZipException {
		DocEntire entity = new DocEntire();
		entity.getDoc().setTitle(title);
		entity.getDoc().setTagkey(tags);
		entity.getDoc().setDocgroupid(groupId);
		entity.setTexts(describe, user);
		entity.getDoc().setReadpop(POP_TYPE_read.getValue());
		entity.getDoc().setWritepop(POP_TYPE_write.getValue());
		entity.getDoc().setDomtype("3");
		FarmDocfile file = farmFileManagerImpl.getFile(zipfileId);
		if (!isHttpZip(file)) {// 验证zip是否可以解压是否有index.html
			throw new NotIsHttpZipException();
		}
		entity.setType(farmDocTypeManagerImpl.getType(type));
		entity.addFile(file);
		entity = farmDocManagerImpl.createDoc(entity, user);
		HttpWebSite website = new HttpWebSite(entity.getDoc());
		try {
			website.indexUrl = initWebSite(file);
		} catch (IOException e) {
			throw new RuntimeException();
		}
		return website;
	}

	private boolean isHttpZip(FarmDocfile fileId) {
		if (fileId.getExname().toUpperCase().equals(".ZIP")) {
			return true;
		}
		return false;
	}

	@Override
	@Transactional
	public String initWebSite(FarmDocfile zipfile) throws IOException {
		String directoryPath = FarmParameterService.getInstance().getParameter("farm.constant.webroot.path")
				+ File.separator + "httpsite" + File.separator + zipfile.getCtime().substring(0, 6) + File.separator
				+ zipfile.getId() + File.separator;// 解压目录
		if ((new File(directoryPath + "index.html")).isFile()) {
			return "httpsite" + "/" + zipfile.getCtime().substring(0, 6) + "/" + zipfile.getId() + "/" + "index.html";
		}
		try {
			ZipUtils.unHtmlZipFiles(zipfile, directoryPath);
		} catch (IOException ex) {
			throw ex;
		}
		if (!(new File(directoryPath + "index.html")).isFile()) {
			throw new RuntimeException("压缩文件中未包含index.html文件");
		}
		return "httpsite" + "/" + zipfile.getCtime().substring(0, 6) + "/" + zipfile.getId() + "/" + "index.html";
	}

	@Override
	@Transactional
	public HttpWebSite getWebSite(String docid, LoginUser user) {
		HttpWebSite website = null;
		try {
			website = new HttpWebSite(farmDocManagerImpl.getDocOnlyBean(docid));
			// 获得站点首页地址
			for (FarmDocfile file : farmFileManagerImpl.getAllFileForDoc(website.getDoc().getId())) {
				if (file.getExname().toUpperCase().equals(".ZIP")) {
					website.indexUrl = initWebSite(file);
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return website;
	}

	@Override
	@Transactional
	public HttpWebSite editSite(String docid, String zipfileId, String knowtag, String text, LoginUser aloneUser,
			String editNote) throws NotIsHttpZipException, CanNoWriteException {
		Doc doc = farmDocManagerImpl.getDocOnlyBean(docid);
		return editSite(docid, zipfileId, doc.getTitle(), knowtag, null, POP_TYPE.getEnum(doc.getReadpop()),
				POP_TYPE.getEnum(doc.getWritepop()), text, aloneUser, doc.getDocgroupid(), editNote);
	}

	@SuppressWarnings("deprecation")
	@Override
	@Transactional
	public HttpWebSite editSite(String docid, String zipfileId, String knowtitle, String knowtag, String typeid,
			POP_TYPE POP_TYPE_read, POP_TYPE POP_TYPE_write, String text, LoginUser aloneUser, String groupId,
			String editNote) throws NotIsHttpZipException, CanNoWriteException {

		FarmDocfile file = farmFileManagerImpl.getFile(zipfileId);
		// 验证zip是否可以解压是否有index.jsp
		if (!isHttpZip(file)) {
			throw new NotIsHttpZipException();
		}
		DocEntire entity = farmDocManagerImpl.getDoc(docid);
		{
			entity.getDoc().setDocgroupid(groupId);
			entity.getDoc().setTitle(knowtitle);
			entity.getDoc().setTagkey(knowtag);
			entity.setTexts(text, aloneUser);
			entity.getDoc().setReadpop(POP_TYPE_read.getValue());
			entity.getDoc().setWritepop(POP_TYPE_write.getValue());
			entity.getDoc().setDomtype("3");
			entity.getDoc().setDocdescribe(text.length() > 250 ? text.substring(0, 250) : text);
			entity.setType(farmDocTypeManagerImpl.getType(typeid));
			entity.addFile(file);
			entity = farmDocManagerImpl.editDocByUser(entity, editNote, aloneUser);
		}
		HttpWebSite website = new HttpWebSite(entity.getDoc());
		try {
			website.indexUrl = initWebSite(file);
		} catch (IOException e) {
			throw new RuntimeException();
		}
		return website;
	}

}
